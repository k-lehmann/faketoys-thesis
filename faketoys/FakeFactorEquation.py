from faketoys import EquationBase
from faketoys import MatrixEquation
from faketoys.stats import dist_base, derived_dist_base, dists, derived_dists
import numpy as np

class FakeFactorEquation(EquationBase.EquationBase, derived_dist_base.DerivedDistributionBase):
    # Inputs for the Fake Factor equation:
    #   - M   fake factors (M = nLeptons)
    #   - 2^M real yields in all measured regions (MC)
    #   - 2^M yields in all measured regions except all tight

    def initializeTo(self, value):
        for trueTypes in self.getTrueTypesList():
            self.trueVector[trueTypes] = value
            self.N[trueTypes] = {}
            for measuredTypes in self.getMeasuredTypesList():
                self.N[trueTypes][measuredTypes] = value
                if set(trueTypes) == set("r"):
                    self.measuredVector[measuredTypes] = value
        for lepIndex in range(self.nLeptons):
            self.F.append(value)
        return

    def __init__(self, nLeptons, name = None):
        EquationBase.EquationBase.__init__(self, nLeptons)
        derived_dist_base.DerivedDistributionBase.__init__(self, name = name)

        # fake factors
        self.F = []
        # MC and data yields
        # first index: true types, second index: measured types
        self.N = {}
        # sums of columns and rows of N
        self.trueVector = {}
        self.measuredVector = {}
        # sum of entire matrix
        self.overallYield = None
        self.initializeTo(None)
        self.distributionsLoaded = False
        return

    @classmethod
    def fromMatrixEquation(cls, matrixEquation, name = None):
        # Create an instance of FakeFactorEquation from an instance of
        # MatrixEquation. It returns None in case consistency checks
        # fail.
        if not isinstance(matrixEquation, MatrixEquation.MatrixEquation):
            print("ERROR (fromMatrixEquation): Pass an instance of MatrixEquation to method fromMatrixEquation(...). Returning None.")
            return None

        if name == None:
            name = "automatically created from ME"
        instance = cls(matrixEquation.nLeptons, name = name)
        # copy all member variables
        for i in range(matrixEquation.nLeptons):
            distr = dists.Delta(matrixEquation.getFakeFactor(i), name = f"FF{i}")
            instance.setFakeFactor(i, distr)

        for trueTypes in instance.getTrueTypesList():
            for measuredTypes in instance.getMeasuredTypesList():
                trueValue = matrixEquation.getNEvents(trueTypes, measuredTypes)
                distr = dists.Delta(trueValue, name = "N" + trueTypes + measuredTypes)
                instance.setNEvents(trueTypes, measuredTypes, distr, checkConsistency = False)
        for trueTypes in instance.getTrueTypesList():
            trueValue = matrixEquation.getNEvents(trueTypes, "")
            distr = dists.Delta(trueValue, name = "N" + trueTypes)
            instance.setNEvents(trueTypes, "", distr, checkConsistency = False)
        for measuredTypes in instance.getMeasuredTypesList():
            trueValue = matrixEquation.getNEvents("", measuredTypes)
            distr = dists.Poisson(trueValue, name = "N" + measuredTypes)
            instance.setNEvents("", measuredTypes, distr, checkConsistency = False)
        trueValue = matrixEquation.getNEvents("", "")
        distr = dists.Delta(trueValue, name = "N")
        if not instance.setNEvents("", "", distr):
            instance.log.WARNING("Consistency check failed when converting MatrixEquation into FakeFactorEquation. Returning None.")
            return None
        return instance

    def __repr__(self):
        if self.name == None:
            return f"FakeFactorEquation({self.nLeptons}) with variables:\n{self.getStoredValuesAsString()}"
        else:
            return f"FakeFactorEquation({self.nLeptons}, {self.name}) with variables:\n {self.getStoredValuesAsString()}"

    def getStoredValuesAsString(self):
        return f"N = {self.N}\ntrueVector = {self.trueVector}\nmeasuredVector = {self.measuredVector}\nF = {self.F}"

    def printStoredValues(self):
        print("Printing stored values\n" + self.getStoredValuesAsString())



    ### Methods related to storage of yields and fake factors ###

    def readNEvents(self, trueTypes, measuredTypes):
        # Retrieves number of events that are set with setNEvents(). If this
        # value was not set with setNEvents() or the input types are invalid,
        # this method returns None.
        # See also getNEvents(), which tries to construct the requested
        # distribution from other stored distributions.
        trueMeasuredCategory = self.analyzeTrueAndMeasuredTypes(trueTypes, measuredTypes)
        if trueMeasuredCategory == EquationBase.TrueMeasuredCategory.INVALID:
            FakeFactorEquation.WARNING("Returning from getNEvents prematurely. The arguments are not correct.")
            return None
        if trueMeasuredCategory == EquationBase.TrueMeasuredCategory.TRUEANDMEASURED:
            FakeFactorEquation.DEBUG(f"readNEvents() Returning event yield from matrix {self.N[trueTypes][measuredTypes]}")
            return self.N[trueTypes][measuredTypes]
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.TRUE:
            FakeFactorEquation.DEBUG(f"readNEvents() Returning event yield from trueVector {self.trueVector[trueTypes]}")
            return self.trueVector[trueTypes]
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.MEASURED:
            FakeFactorEquation.DEBUG(f"readNEvents() Returning event yield from measuredVector {self.measuredVector[measuredTypes]}")
            return self.measuredVector[measuredTypes]
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.OVERALL:
            FakeFactorEquation.DEBUG(f"readNEvents() Returning event yield from overall yield {self.overallYield}")
            return self.overallYield
        FakeFactorEquation.ERROR("This case should never happen. There seems to be a logical error somewhere.")
        return None

    def setNEvents(self, trueTypes, measuredTypes, distribution, checkConsistency = True):
        # Set the number of events of a combintaion of true types (r,f) and
        # measured types (t,l). Since the way of storing information internally
        # can contain redudant information, a consistency check is performed
        # after setting a distribution (disable with checkConsistency). The return
        # value is True if everything is consistent or if no check is performed,
        # else False. After this method, loadDistributions() needs to be called
        # before equation can be applied.
        if not isinstance(distribution, dist_base.DistributionBase) and not distribution == None:
            FakeFactorEquation.ERROR(f"setNEvents() expects instance of DistributionBase as argument 'distribution'. Got '{distribution}' (type {type(distribution)}) instead.")
            return False
        trueMeasuredCategory = self.analyzeTrueAndMeasuredTypes(trueTypes, measuredTypes)
        if trueMeasuredCategory == EquationBase.TrueMeasuredCategory.INVALID:
            FakeFactorEquation.WARNING("Returning from setNEvents prematurely. The arguments ('{trueTypes}', '{measuredTypes}') are not correct.")
            return False
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.OVERALL:
            self.overallYield = distribution
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.TRUE:
            self.trueVector[trueTypes] = distribution
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.MEASURED:
            self.measuredVector[measuredTypes] = distribution
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.TRUEANDMEASURED:
            self.N[trueTypes][measuredTypes] = distribution
        else:
            FakeFactorEquation.ERROR("This case should never happen. There seems to be a logical error somewhere.")
            return False
        self.distributionsLoaded = False
        FakeFactorEquation.DEBUG(f"Successfully set variable. No{'w' if checkConsistency else 't'} checking for consistency.")
        return not checkConsistency or self.checkConsistency()

    def getFakeFactor(self, leptonIndex):
        return self.F[leptonIndex]

    def setFakeFactor(self, leptonIndex, F):
        # Set fake factor F for lepton with leptonIndex. Returns False
        # if the fake factor can't be set, True otherwise. After this
        # method, loadDistributions() needs to be called before
        # equation can be applied.
        if leptonIndex >= self.nLeptons or leptonIndex < 0:
            return False
        self.distributionsLoaded = False
        if F == None:
            FakeFactorEquation.DEBUG("Setting fake factor {leptonIndex} to None")
        elif not isinstance(F, derived_dists.FakeFactor):
            FakeFactorEquation.DEBUG(f"Setting fake factor to something that doesn't look like one: {F} of type {type(F)}")
        elif not isinstance(F, dist_base.DistributionBase):
            FakeFactorEquation.WARNING(f"Setting fake factor to something that doesn't even look like a distribution. This will cause problems when trying to evaluate the fake yield.")
        self.F[leptonIndex] = F
        return True

    def checkConsistency(self):
        # Checks if the inputs defined by setNEvents() are consistent using true
        # values of all distributions. The fake factor equation is not tested.
        FakeFactorEquation.DEBUG("Entering checkConsistency()")
        consistent = True
        for trueTypes in (self.getTrueTypesList() + [""]):
            for measuredTypes in (self.getMeasuredTypesList() + [""]):
                l = []
                l.append(dist_base.DistributionBase.evaluateDistribution(self.readNEvents(trueTypes, measuredTypes), dist_base.EvaluationMode.TRUEVALUE))
                l.append(dist_base.DistributionBase.evaluateDistribution(self.createNEventsFromMatrix(trueTypes, measuredTypes), dist_base.EvaluationMode.TRUEVALUE))
                l.append(dist_base.DistributionBase.evaluateDistribution(self.createNEventsFromTrueVector(trueTypes, measuredTypes), dist_base.EvaluationMode.TRUEVALUE))
                l.append(dist_base.DistributionBase.evaluateDistribution(self.createNEventsFromMeasuredVector(trueTypes, measuredTypes), dist_base.EvaluationMode.TRUEVALUE))
                l = [x for x in l if x != None]
                warningPrinted = False
                for l1 in l:
                    if warningPrinted:
                        break
                    for l2 in l:
                        if not EquationBase.EquationBase.valuesEqual(l1, l2):
                            consistent = False
                            FakeFactorEquation.WARNING(f"Consistency error found for NEvents('{trueTypes}', '{measuredTypes}'). The ambiguous values are {l}.")
                            warningPrinted = True
                            break
        if not consistent:
            FakeFactorEquation.DEBUG(f"For debugging, see below the stored values in {self}:\n" + self.getStoredValuesAsString())
        FakeFactorEquation.DEBUG(f"checkConsistency() is returning {consistent}")
        return consistent


    ### Methods to create new distributions if not available otherwise ###

    def createNEventsFromMatrix(self, trueTypes, measuredTypes):
        # Pass true and measured types to sum over the stored matrix and try to
        # construct a distribution for their combination. If no distribution can
        # be constructed or the inputs don't make any sense, None is returned.
        # Note that None is even returned if readNEvents() was able to return the
        # nominal, stored distribution.
        trueMeasuredCategory = self.analyzeTrueAndMeasuredTypes(trueTypes, measuredTypes)

        distributionsToSum = []
        if trueMeasuredCategory == EquationBase.TrueMeasuredCategory.INVALID:
            FakeFactorEquation.DEBUG("EquationBase.TrueMeasuredCategory INVALID. Returning None.")
            return None
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.OVERALL:
            FakeFactorEquation.DEBUG("Trying to construct overall yield from matrix.")
            name = "N (auto-gen)"
            for trueTypes in self.getTrueTypesList():
                for measuredTypes in self.getMeasuredTypesList():
                    if self.N[trueTypes][measuredTypes] == None:
                        FakeFactorEquation.DEBUG(f"Found undefined entry for '{trueTypes}', '{measuredTypes}'. Returning None.")
                        return None
                    distributionsToSum.append(self.N[trueTypes][measuredTypes])
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.TRUE:
            name = f"N{trueTypes} (auto-gen)"
            for measuredTypes in self.getMeasuredTypesList():
                if self.N[trueTypes][measuredTypes] == None:
                    FakeFactorEquation.DEBUG(f"Found undefined entry for '{trueTypes}', '{measuredTypes}'. Returning None.")
                    return None
                distributionsToSum.append(self.N[trueTypes][measuredTypes])
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.MEASURED:
            name = f"N{measuredTypes} (auto-gen)"
            for trueTypes in self.getTrueTypesList():
                if self.N[trueTypes][measuredTypes] == None:
                    FakeFactorEquation.DEBUG(f"Found undefined entry for '{trueTypes}', '{measuredTypes}'. Returning None.")
                    return None
                distributionsToSum.append(self.N[trueTypes][measuredTypes])
        elif trueMeasuredCategory == EquationBase.TrueMeasuredCategory.TRUEANDMEASURED:
            FakeFactorEquation.DEBUG("Trying to create nEvents from matrix using TRUEANDMEASURED input types. This is a bit pointless. If you want to retrieve the nominal stored value, use readNEvents(). Returning None.")
            return None
        else:
            FakeFactorEquation.ERROR("This line should never be executed. Internal inconsistency.")
            return None
        distr = derived_dists.SumOfDistributions(distributionsToSum, name = name)
        FakeFactorEquation.DEBUG(f"createNEventsFromTrueMatrix() returning {distr}")
        return distr

    def createNEventsFromTrueVector(self, trueTypes, measuredTypes):
        # Method analogous to createNEventsFromMatrix()
        trueMeasuredCategory = self.analyzeTrueAndMeasuredTypes(trueTypes, measuredTypes)

        distributionsToSum = []
        if trueMeasuredCategory == EquationBase.TrueMeasuredCategory.OVERALL:
            FakeFactorEquation.DEBUG("Trying to construct overall yield from true vector.")
            for trueTypes in self.getTrueTypesList():
                if self.trueVector[trueTypes] == None:
                    FakeFactorEquation.DEBUG(f"Found undefined vector entry for '{trueTypes}'. Returning None.")
                    return None
                distributionsToSum.append(self.trueVector[trueTypes])
        else:
            FakeFactorEquation.DEBUG(f"Trying to construct yield from true vector, with {trueMeasuredCategory} input types. If you want to retrieve the nominal stored value, use readNEvents(). Returning None.")
            return None
        distr = derived_dists.SumOfDistributions(distributionsToSum, name = "N (auto-gen)")
        FakeFactorEquation.DEBUG(f"createNEventsFromTrueVector() returning {distr}")
        return distr

    def createNEventsFromMeasuredVector(self, trueTypes, measuredTypes):
        # Method analogous to createNEventsFromMatrix()
        trueMeasuredCategory = self.analyzeTrueAndMeasuredTypes(trueTypes, measuredTypes)

        distributionsToSum = []
        if trueMeasuredCategory == EquationBase.TrueMeasuredCategory.OVERALL:
            FakeFactorEquation.DEBUG("Trying to construct overall yield from measured vector.")
            for measuredTypes in self.getMeasuredTypesList():
                if self.measuredVector[measuredTypes] == None:
                    FakeFactorEquation.DEBUG(f"Found undefined vector entry for '{measuredTypes}'. Returning None.")
                    return None
                distributionsToSum.append(self.measuredVector[measuredTypes])
        else:
            FakeFactorEquation.DEBUG(f"Trying to construct yield from measured vector, with {trueMeasuredCategory} input types. If you want to retrieve the nominal stored value, use readNEvents(). Returning None.")
            return None
        distr = derived_dists.SumOfDistributions(distributionsToSum, name = "N (auto-gen)")
        FakeFactorEquation.DEBUG(f"createNEventsFromMeasuredVector() returning {distr}")
        return distr

    def getNEvents(self, trueTypes, measuredTypes):
        # Retrieves the number of events defined by trueTypes and measuredTypes.
        # If the yield has been set with setNEvents(), this method returns the
        # same value as readNEvents(). If it was not set, it tries to construct
        # a distribution based on other stored information. Eg. if in a
        # two-lepton FakeFactorEquation, the yield for Nrr was not defined, then
        # the sum of Nrrll, Nrrlt, Nrrtl, Nrrtt is constructed and returned.
        FakeFactorEquation.DEBUG("Entering getNEvents()")
        if not self.checkTrueAndMeasuredTypes(trueTypes, measuredTypes, allowEmpty = True):
            FakeFactorEquation.WARNING("Returning from createNEvents prematurely. The arguments are not correct.")
            return None
        nEvents = self.readNEvents(trueTypes, measuredTypes)
        if not nEvents == None:
            FakeFactorEquation.DEBUG(f"A distribution for '{trueTypes}', '{measuredTypes}' was found: {nEvents}. No need to create one. Returning it directly.")
            return nEvents

        FakeFactorEquation.INFO(f"The distribution '{trueTypes}', '{measuredTypes}' is not available. Trying to construct it now.")
        distr = {}
        distr["nEventsFromMatrix"]         = self.createNEventsFromMatrix(trueTypes, measuredTypes)
        distr["nEventsFromTrueVector"]     = self.createNEventsFromTrueVector(trueTypes, measuredTypes)
        distr["nEventsFromMeasuredVector"] = self.createNEventsFromMeasuredVector(trueTypes, measuredTypes)

        nDistr = sum(value != None for value in distr.values())
        if nDistr == 0:
            FakeFactorEquation.DEBUG("No distributions could be constructed. Returning None.")
            return None

        # One or more distributions found. Check if they are consistent.
        trueValues = dict([(key, value.getTrueValue()) for (key, value) in distr.items() if value != None])
        if not len(set(trueValues.values())) == 1:
            FakeFactorEquation.WARNING(f"Discovered inconsistency in the stored data. Different distributions were constructed for the types '{trueTypes}', '{measuredTypes}' that do not share the same true value:")
            FakeFactorEquation.WARNING(f"{trueValues}")
            FakeFactorEquation.WARNING("Returning None.")
            return None

        # Estimate the stddev of all distributions and return the distribution
        # with the smallest one.
        if nDistr > 1:
            FakeFactorEquation.DEBUG(f"Found {nDistr} distributions. Finding the distribution with the smallest uncertainties now.")
        stddevs = dict([(key, value.getStddev()) for (key, value) in distr.items() if value != None])
        minKey = min(stddevs.keys(), key=stddevs.get)
        FakeFactorEquation.DEBUG(f"The distribution with the smallest Gaussian uncertainty is returned: {minKey}, {distr[minKey]}")
        return distr[minKey]



    ### Methods to make the implementation above talk with DerivedDistributionBase ###

    def unloadDistributions(self):
        self.unsetUnderlyingDistributions()
        if not self.distributionsLoaded:
            return False
        self.distributionsLoaded = False
        return True

    def loadDistributions(self, printWarnings = False):
        # Retrieve/construct all necessary distributions from internal
        # storage that are needed to calculate fake yield. This method
        # needs to be called before the equation can be applied.
        # Returns True if successful, False otherwise.
        FakeFactorEquation.DEBUG("Entering loadDistributions()")
        if self.distributionsLoaded:
            # To suppress this message, call unloadDistributions() first
            FakeFactorEquation.INFO("Reloading distributions. This is not a problem, but shouldn't be necessary and just consume time.")

        # Here we use a flat dictionary instead of a nested one. This
        # allows to use the methods of DerivedDistributionBase.
        # Indices are:
        # - rrtt (true, true, measured, measured) for combined yields
        # - rr (true, true) if only true indices are given
        # - FF_i with i=0,1... for FFs with index i
        distributions = {}
        success = True
        allReal = self.nLeptons*"r"
        allTight = self.nLeptons*"t"
        for measuredTypes in self.getMeasuredTypesList():
            if measuredTypes == allTight:
                # Don't need yields from SR
                continue
            distr = self.getNEvents(allReal, measuredTypes)
            distributions[''.join([allReal, measuredTypes])] = distr
            if distr == None:
                FakeFactorEquation.DEBUG(f"(loadDistributions) Number of events with real leptons and measured types '{measuredTypes}' is not defined. Returning false.")
                if printWarnings:
                    FakeFactorEquation.WARNING(f"(loadDistributions) Number of events with real leptons and measured types '{measuredTypes}' is not defined. Returning false.")
                success = False
            distr = self.getNEvents("", measuredTypes)
            distributions[measuredTypes] = distr
            if distr == None:
                FakeFactorEquation.DEBUG(f"(loadDistributions) Number of events with measured types '{measuredTypes}' is not defined. Returning false.")
                if printWarnings:
                    FakeFactorEquation.WARNING(f"(loadDistributions) Number of events with measured types '{measuredTypes}' is not defined. Returning false.")
                success = False

        # Load fake factors now
        if not len(self.F) == self.nLeptons:
            FakeFactorEquation.DEBUG(f"(loadDistributions) Number of leptons {self.nLeptons} is not equal to number of FFs {len(self.F)}. Returning false.")
            if printWarnings:
                FakeFactorEquation.ERROR(f"(loadDistributions) Number of leptons {self.nLeptons} is not equal to number of FFs {len(self.F)}. This should never happen. Returning false right now.")
            # Don't continue here because the following loop can crash
            return False
        for leptonIndex in range(self.nLeptons):
            if (not isinstance(self.F[leptonIndex], dist_base.DistributionBase)):
                FakeFactorEquation.DEBUG(f"(loadDistributions) fake factor {leptonIndex} has type {type(self.F[leptonIndex])}. Expected DistributionBase. Returning false.")
                if printWarnings:
                    FakeFactorEquation.WARNING(f"(loadDistributions) fake factor {leptonIndex} has type {type(self.F[leptonIndex])}. Expected DistributionBase. Returning false.")
                success = False
            distributions[f"FF_{leptonIndex}"] = self.F[leptonIndex]

        # Set underlying distributions even if an error was found
        if success:
            self.distributionsLoaded = True
        self.setUnderlyingDistributions(distributions)
        FakeFactorEquation.DEBUG(f"loadDistributions() is returning {success}")
        return success


    def calculateValue(self, dictOfValues):
        # Use Fake Factor equation to calculate fake yield as number of events
        # with only tight leptons minus number of events with only tight, real
        # leptons. Only inputs to the FF equation are used. If the inputs are
        # not present, return None.

        fakeYield = 0.
        allReal = self.nLeptons*"r"
        for measuredTypes in self.getMeasuredTypesList():
            FakeFactorEquation.DEBUG(f"calculating combination {measuredTypes}")
            if set(measuredTypes) == set("t"):
                FakeFactorEquation.DEBUG(f"skipping combination {measuredTypes}")
                continue
            term = dictOfValues[''.join([allReal, measuredTypes])] - dictOfValues[measuredTypes]
            FakeFactorEquation.DEBUG(f"getting event yield {term} (expected to be negative)")
            for lepIndex in range(self.nLeptons):
                if measuredTypes[lepIndex] == "l":
                    FakeFactorEquation.DEBUG(f"multiplying fake factor of lepton {lepIndex}")
                    term = - term * dictOfValues[f"FF_{lepIndex}"]
            FakeFactorEquation.DEBUG(f"final term = {term}")
            fakeYield = fakeYield + term
        return fakeYield
