#!/usr/bin/env python3

import pickle
import xarray as xr
import numpy as np
import scipy
import argparse
from faketoys import utils, log
import dask.diagnostics

def getRandom(distribution, nRandom, seed=None):
    kwargs = {}
    if not seed == None:
        kwargs = dict(rng = np.random.default_rng(int(seed)))
    return distribution.getRandom(nRandom=nRandom, **kwargs)

def quadratureSumSeparately(random, mean):
    """Sum entries of random in quadrature after subtracting mean.

    Expects a list for random. The entries are added in quadrature for
    up, down variations separately. Correspondingly, two numbers are
    returned in the order "up, down". Both numbers are positive.
    """
    up   = np.sqrt(sum([(x-mean)**2 for x in random if (x-mean > 0)]))
    down = np.sqrt(sum([(x-mean)**2 for x in random if (x-mean < 0)]))
    return up, down

def asymStddev(random, mean):
    '''Calculate standard deviation separately for up and down.

    Using the common definition::

      standard deviation = sum_i (x_i - mean)^2

    separately for the two cases x_i > mean and x_i < mean. When the
    random value x_i = mean, this case is counted towards the up and
    down standard deviation in equal parts. For both return values,
    the result is reported as a positive value.
    '''
    # asymmetric stddev
    ups   = [(x-mean)**2 for x in random if (x-mean > 0)]
    downs = [(x-mean)**2 for x in random if (x-mean < 0)]
    zeros = [(x-mean)**2 for x in random if (x-mean == 0)]
    if len(ups) + len(zeros) == 0:
        vUp = 0
    else:
        vUp   = sum(ups)   / (len(ups) + len(zeros)/2.)
    if len(downs) + len(zeros) == 0:
        vDown = 0
    else:
        vDown = sum(downs) / (len(downs) + len(zeros)/2.)
    stddevUp = np.sqrt(vUp)
    stddevDown = np.sqrt(vDown)
    return (stddevUp, stddevDown)

def asymStddevGreater0(random, mean):
    '''Like asymStddev, but ignoring negative values.'''
    r = [x if x > 0 else 0 for x in random]
    return asymStddev(r, mean)

def getPValue(trueValue, randomValue, stddevUp, stddevDown):
    """Calculate p-value assuming Gaussian distribution.

    Construct Gaussian pdf with mean at randomValue and standard
    deviation stddevDown/stddevUp if the true value is smaller/larger
    than randomValue. Returns the integral from negative infinity to
    trueValue.
    """
    try:
        if not np.isfinite(randomValue) or not np.isfinite(trueValue):
            return np.nan
    except(TypeError):
        # This happens when randomValue (type int) becomes too large.
        # Simply return 0.
        return 0

    if trueValue == randomValue:
        return 0.5
    if trueValue < randomValue:
        if stddevDown == 0:
            return 0
        z = (trueValue - randomValue) / stddevDown
    elif trueValue > randomValue:
        if stddevUp == 0:
            return 1
        z = (trueValue - randomValue) / stddevUp
    else:
        log.ERROR(f"Internal inconsistency in compute.getPValue(...) with \
        arguments ({trueValue}, {randomValue}, {stddevUp}, {stddevDown}).")
    # scipy.special.erf(z) returns
    #   2/sqrt(pi)*integral(exp(-t**2), t=0..z)
    # Convert it into the cumulative of the unit distribution
    pValue = 1/2. * (1 + scipy.special.erf(z / np.sqrt(2)))
    return pValue

def getRandomAndPValues(distribution, nRandom=1, seed=None, cacheIndex=None):
    '''Get a dictionary of lists of random and p values.

    The p values are calculated by varying underlying sources of
    uncertainty by +/- 1 standard deviation and combining the propagated
    results in quadrature separately for postitive and negative
    variations.

    If used with nRandom > 1, multiple pairs of random and p-values are
    created. If used with cacheIndex, then a pair of random and p-values
    is only created once. The output is still a dictionary of lists.
    '''
    kwargs = {}
    if not seed == None:
        kwargs = dict(rng = np.random.default_rng(int(seed)))

    if nRandom > 1 and not cacheIndex == None:
        log.ERROR("getRandomAndPValues(...): 'nRandom' or 'cacheIndex' need to be the default value.")
        return {"random": [], "pValue": []}
    systSources = distribution.findAllSources()
    randomValues = []
    pValues = []
    cIndex = cacheIndex
    for iteration in range(nRandom):
        if cacheIndex == None:
            cIndex = type(distribution).getAndUpdateSmallestUnusedCacheIndex()
        randomValue = distribution.getRandom(cacheIndex=cIndex, systSources=systSources, statUnc=True, **kwargs)
        listOfVariedValues = distribution.getListOfVariedValues(cacheIndex=cIndex, systSources=systSources, statUnc=True)
        sigmaUp, sigmaDown = quadratureSumSeparately(listOfVariedValues, randomValue)
        pValue = getPValue(distribution.getTrueValue(), randomValue, sigmaUp, sigmaDown)
        randomValues.append(randomValue)
        pValues.append(pValue)
    return {"random": randomValues, "pValue": pValues}

def getRandomAndPValues_linearErrorPropagation(distribution, nRandom=1, seed=None, cacheIndex=None):
    '''Get a dictionary of lists of random and p values.
    '''
    kwargs = {}
    if not seed == None:
        kwargs = dict(rng = np.random.default_rng(int(seed)))

    if nRandom > 1 and not cacheIndex == None:
        log.ERROR("getRandomAndPValues(...): 'nRandom' or 'cacheIndex' need to be the default value.")
        return {"random": [], "pValue": []}
    systSources = distribution.findAllSources()
    randomValues = []
    pValues = []
    cIndex = cacheIndex
    for iteration in range(nRandom):
        if cacheIndex == None:
            cIndex = type(distribution).getAndUpdateSmallestUnusedCacheIndex()
        expression = distribution.getRandom(cacheIndex=cIndex, systSources=systSources, statUnc=True, expression=True, **kwargs)
        pValue = getPValue(distribution.getTrueValue(), expression.n, expression.s, expression.s)
        randomValues.append(expression.n)
        pValues.append(pValue)
    return {"random": randomValues, "pValue": pValues}


def histogram(x, bins, underOverflowX=False):
    if not underOverflowX:
        return np.histogram(x, bins=bins)
    newBins = [-np.inf, *bins, np.inf]
    weights, _ = np.histogram(x, bins=newBins)
    weights[1] += weights[0]
    weights[-2] += weights[-1]
    weights = weights[1:-1]
    return weights, bins

def histogram2d(x, y, binsX, binsY, underOverflowX=False, underOverflowY=False):
    newBinsX = binsX
    if underOverflowX:
        newBinsX = [-np.inf, *binsX, np.inf]
    newBinsY = binsY
    if underOverflowY:
        newBinsY = [-np.inf, *binsY, np.inf]
    weights, _, _ = np.histogram2d(x, y, bins=(newBinsX, newBinsY))
    if underOverflowX:
        for y in range(0, weights.shape[1]):
            weights[1, y] += weights[0, y]
            weights[-2, y] += weights[-1, y]
        weights = weights[1:-1, :]
    if underOverflowY:
        for x in range(0, weights.shape[0]):
            weights[x, 1] += weights[x, 0]
            weights[x, -2] += weights[x, -1]
        weights = weights[:, 1:-1]
    return weights, (binsX, binsY)

def manualHistogramBinning(low, high, step, dummy=None):
    # dummy can be passed to force xarray to vectorize
    binning = np.arange(low, high+step, step)
    return binning

def smartHistogramBinning(mean, stddev, nBins=100, nStddev=4, random=None):
    '''Calculate histogram boundaries using mean and standard deviation.

    The parameters mean and stddev take numbers. Alternatively, stddev
    can also be a tuple/list of length 2. The first entry is the up
    variation, the second the down variation. Both entries are positive.
    nBins is the number of bins and nStddev the number of standard
    deviations from the mean. The parameter random is optional and takes
    the unbinned list of values. If given, the histogram range does not
    extend past the maximum and minimum of the list.
    '''
    if isinstance(stddev, tuple) or isinstance(stddev, list):
        if len(stddev) > 2:
            log.WARNING("smartHistogramBinning(): stddev expects a tuple/list of (up, down) variations or a number if the standard deviation is symmetric. Using only the first two entries.")
        stddevUp = stddev[0]
        if len(stddev) >= 2:
            stddevDown = stddev[1]
        if len(stddev) == 1:
            stddevDown = stddev[0]
    else:
        stddevUp = stddev
        stddevDown = stddev
    if stddevUp <= 0 or stddevDown <= 0:
        log.WARNING(f"stddevUp ({stddevUp}) and stddevDown ({stddevUp}) are expected to be positive.")
    high = mean + nStddev * stddevUp
    low = mean - nStddev * stddevDown

    if not random == None:
        ma = max(random)
        mi = min(random)
        if ma < high:
            high = ma
        if mi > low:
            low = mi
    binning = np.linspace(low, high, num=nBins+1)
    return binning

def getVariablesToCompute(conf):
    d_vectorize = {
        "vectorize": True,
        }
    d_vectorize_object = d_vectorize.copy()
    d_vectorize_object["output_dtypes"] = [np.dtype('object')]

    functions = {}
    functions["trueValue"] = (
        lambda x: x.getTrueValue(),
        ["ds.equation"],
        d_vectorize
        )
    functions["listOfVariedValues"] = (
        lambda x: x.getListOfVariedValues(),
        ["ds.equation"],
        d_vectorize_object
        )
    functions["expression"] = (
        lambda x: x.getExpression(),
        ["ds.equation"],
        d_vectorize_object
        )
    functions["mean"] = (
        lambda x: sum(x) / len(x),
        ["random"],
        d_vectorize
        )
    functions["mean_unc"] = (
        lambda x: scipy.stats.sem(x),
        ["random"],
        d_vectorize
        )
    functions["stddev"] = (
        lambda x: np.std(x),
        ["random"],
        d_vectorize
        )
    functions["median"] = (
        lambda x: np.median(x),
        ["random"],
        d_vectorize
        )
    functions["asymStddev"] = (
        asymStddev,
        ["random", "ds['mean']"],
        d_vectorize_object
        )
    functions["asymStddevGreater0"] = (
        asymStddevGreater0,
        ["random", "ds['mean']"],
        d_vectorize_object
        )

    if conf.smartBins_random:
        functions["random_binning"] = (
            smartHistogramBinning,
            ["ds['mean']", "ds['asymStddev']", "conf.smartBins_nBins", "conf.smartBins_nStddev", "random"],
            d_vectorize_object
            )
    else:
        functions["random_binning"] = (
            manualHistogramBinning,
            [str(conf.randomHistLowX), str(conf.randomHistHighX), str(conf.randomHistStepX), "random"],
            d_vectorize_object
            )

    functions["h_random"] = (
        lambda x, binsX: histogram(x, binsX, underOverflowX=True),
        ["random", "ds['random_binning']"],
        d_vectorize_object
        )
    if "pValueHistStepX" in dir(conf):
        pValueHistogramBins = manualHistogramBinning(0, 1, conf.pValueHistStepX)
        functions["h_pValue"] = (
            lambda x: histogram(x, bins=pValueHistogramBins),
            ["pValue"],
            d_vectorize_object
            )
        functions["h_random_pValue"] = (
            lambda x, y, binsX: histogram2d(x, y, binsX, binsY=pValueHistogramBins, underOverflowX=True),
            ["random", "pValue", "ds['random_binning']"],
            d_vectorize_object
            )
    return functions

def getDictEntry(dictionary, key):
    if not key in dictionary:
        log.WARNING(f"The key {key} is not in the dictionary. Available are {dictionary.keys()}.")
    return dictionary[key]

def getRandomAndPValues_wrapper(ds, conf, args, seeds=None):
    if args.parallelize:
        log.INFO("About to throw parallelized toys (random, pValues)")
        # Split as finely as possible
        split = dict([(key, 1) for key in ds.dims.keys()])
        randomAndPValues = xr.apply_ufunc(getRandomAndPValues,
                                          ds.equation.chunk(split),
                                          conf.compute_nRandom,
                                          seeds,
                                          vectorize=True,
                                          output_dtypes=[np.dtype('object')],
                                          dask='parallelized')
        kwargs = {"scheduler": "processes"}
        if args.num_workers > 0:
            kwargs["num_workers"] = args.num_workers
        with dask.diagnostics.ProgressBar(dt=args.progress_dt):
            randomAndPValues = randomAndPValues.compute(**kwargs)

    else:
        log.INFO("About to throw non-parallelized toys (random, pValues)")
        randomAndPValues = xr.apply_ufunc(getRandomAndPValues,
                                          ds.equation,
                                          conf.compute_nRandom,
                                          seeds,
                                          vectorize=True,
                                          output_dtypes=[np.dtype('object')])

    random = xr.apply_ufunc(getDictEntry,
                            randomAndPValues,
                            "random",
                            vectorize=True,
                            output_dtypes=[np.dtype('object')])
    pValue = xr.apply_ufunc(getDictEntry,
                            randomAndPValues,
                            "pValue",
                            vectorize=True,
                            output_dtypes=[np.dtype('object')])
    return random, pValue

def getRandomAndPValues_linearErrorPropagation_wrapper(ds, conf, args, seeds=None):
    if args.parallelize:
        log.INFO("About to throw parallelized toys (random, pValues (lin. error propag.))")
        # Split as finely as possible
        split = dict([(key, 1) for key in ds.dims.keys()])
        randomAndPValues = xr.apply_ufunc(getRandomAndPValues_linearErrorPropagation,
                                          ds.equation.chunk(split),
                                          conf.compute_nRandom,
                                          seeds,
                                          vectorize=True,
                                          output_dtypes=[np.dtype('object')],
                                          dask='parallelized')
        kwargs = {"scheduler": "processes"}
        if args.num_workers > 0:
            kwargs["num_workers"] = args.num_workers
        with dask.diagnostics.ProgressBar(dt=args.progress_dt):
            randomAndPValues = randomAndPValues.compute(**kwargs)

    else:
        log.INFO("About to throw non-parallelized toys (random, pValues (lin. error propag.))")
        randomAndPValues = xr.apply_ufunc(getRandomAndPValues_linearErrorPropagation,
                                          ds.equation,
                                          conf.compute_nRandom,
                                          seeds,
                                          vectorize=True,
                                          output_dtypes=[np.dtype('object')])

    random = xr.apply_ufunc(getDictEntry,
                            randomAndPValues,
                            "random",
                            vectorize=True,
                            output_dtypes=[np.dtype('object')])
    pValue = xr.apply_ufunc(getDictEntry,
                            randomAndPValues,
                            "pValue",
                            vectorize=True,
                            output_dtypes=[np.dtype('object')])
    return random, pValue


def getRandom_wrapper(ds, conf, args, seeds=None):
    if args.parallelize:
        log.INFO("About to throw parallelized toys (random)")
        # Split as finely as possible
        split = dict([(key, 1) for key in ds.dims.keys()])
        random = xr.apply_ufunc(getRandom,
                                ds.equation.chunk(split),
                                conf.compute_nRandom,
                                seeds,
                                vectorize=True,
                                output_dtypes=[np.dtype('object')],
                                dask='parallelized')
        kwargs = {"scheduler": "processes"}
        if args.num_workers > 0:
            kwargs["num_workers"] = args.num_workers
        with dask.diagnostics.ProgressBar(dt=args.progress_dt):
            random = random.compute(**kwargs)

    else:
        log.INFO("About to throw non-parallelized toys (random)")
        random = xr.apply_ufunc(getRandom,
                                ds.equation,
                                conf.compute_nRandom,
                                seeds,
                                vectorize=True,
                                output_dtypes=[np.dtype('object')])
    return random

def main(args):

    conf = utils.importModuleFromConfig(args.config)
    log.INFO(f"Imported config from {args.config}")

    inputFile = utils.getInputPath(conf.preparedOutputFile)
    ds = utils.readDataset(inputFile)


    # Find out which seed to use for random number generator. This rng
    # is then used to initialize one rng per equation. One equation is
    # the finest possible splitting for parallel jobs. If the random
    # seed is initialized deterministically for each job, the result is
    # is reproducible.
    seed = None

    if "randomSeed" in dir(conf):
        utils.setRandomSeed(conf.randomSeed)
        seed = conf.randomSeed
        log.INFO(f"Using seed {conf.randomSeed} from config file")

    if utils.setRandomSeed(args.random_seed):
        log.INFO(f"Using seed {args.random_seed} to initialize RNG")
        seed = args.random_seed
        if "randomSeed" in dir(conf):
            log.WARNING("Random seed from config file is overwritten!")

    seeds = None
    if not seed == None:
        # This rng is used to generate seeds for individual rngs
        rng = np.random.default_rng(seed)
        seeds = xr.DataArray(coords=ds.coords)
        iinfo64 = np.iinfo(np.int64)
        for loc in utils.getCombinations(seeds):
            seeds.loc[loc] = rng.integers(0, iinfo64.max)


    actions = getVariablesToCompute(conf)
    if "filterActions" in dir(conf):
        actions = {key:value for key, value in actions.items() if key in conf.filterActions}
    generationMode = "randomAndPValues"
    if "generationMode" in dir(conf):
        generationMode = conf.generationMode

    if generationMode == "randomAndPValues":
        random, pValue = getRandomAndPValues_wrapper(ds, conf, args, seeds=seeds)
        log.INFO(f"Done throwing {conf.compute_nRandom} toys for each equation producing random and p-values.")
    elif generationMode == "random":
        random = getRandom_wrapper(ds, conf, args, seeds=seeds)
        log.INFO(f"Done throwing {conf.compute_nRandom} toys for each equation producing random values.")
    elif generationMode == "randomAndPValues_linearErrorPropagation":
        random, pValue = getRandomAndPValues_linearErrorPropagation_wrapper(ds, conf, args, seeds=seeds)
        log.INFO(f"Done throwing {conf.compute_nRandom} toys for each equation producing random and p-values (lin. error propag.).")
    else:
        log.WARNING(f"Fall through: generationMode={generationMode}")

    if args.attachRandom:
        log.INFO("Attaching random variables to output dataset")
        ds = ds.assign(random=random)

    for name, arguments in actions.items():
        log.INFO(f"Processing operation {name}")
        args1 = []
        argsNotFound = []
        for x in arguments[1]:
            try:
                args1.append(eval(x))
            except(NameError):
                argsNotFound.append(x)
        if argsNotFound != []:
            log.WARNING(f"Skipping action action '{name}', because the following args couldn't be found: {','.join(argsNotFound)}")
            continue
        new = xr.apply_ufunc(arguments[0], *args1, **(arguments[2]))
        ds = ds.assign(new=new)
        ds = ds.rename_vars({"new": name})
    if "h_random" in ds:
        ds.h_random.attrs['nRandom'] = conf.compute_nRandom
    if "h_pValue" in ds:
        ds.h_pValue.attrs['nRandom'] = conf.compute_nRandom

    log.INFO("Successfully processed all actions and added result to dataset.")

    utils.decorateDataset(ds, "compute", args)

    outPath = utils.getOutputPath(conf.computedOutputFile)
    with open(outPath, "wb") as f:
        pickle.dump(ds, f, protocol=4)
    log.INFO(f"Wrote Dataset to '{outPath}'")
    return

if __name__ == '__main__':
    parser = utils.getArgParserCompute()
    args = parser.parse_args()
    main(args)
