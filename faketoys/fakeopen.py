#!/usr/bin/env python3
# this is a -*- python -*- file

if __name__ == '__main__':

    import os
    os.environ['PYTHONINSPECT'] = 'TRUE' #interactive mode after execution of the lines below

    from faketoys import utils, log

    import sys
    import re
    from os.path import isfile

    for argi in range(1,len(sys.argv)):
        arg = sys.argv[argi]

        if not re.match("^.*\.pkl", arg) or re.match("^.*\.pickle", arg):
            log.WARNING("Expecting a pickle file ending with '.pkl' or '.pickle'. Trying to open anyway.")

        fileInCentralDir = utils.getInputPath(arg)
        if isfile(arg):
            inputFile = arg
        elif isfile(fileInCentralDir):
            inputFile = fileInCentralDir
        else:
            print(f"no such file: {arg}")
            continue

        filename = f"_file{argi-1}"
        log.INFO("Adding input filename '{:s}' as '{:s}'".format(inputFile,filename))
        globals()[filename] = inputFile

        dsName = "ds"
        if argi > 1:
            dsName = dsName + filename
        log.INFO(f"Adding contained dataset as '{dsName}'")
        globals()[dsName] = utils.readDataset(inputFile, infoMessage=False)

    if len(sys.argv) > 1:
        del globals()['dsName']
        del globals()['inputFile']
        del globals()['fileInCentralDir']
        del globals()['arg']
        del globals()['filename']
    else:
        log.INFO("Environment loaded. No dataset opened, because no file given as command-line argument.")
