import unittest
from faketoys import EquationBase

class TestEquationBase(unittest.TestCase):

    def setUp(self):
        return

    def conversions(self, equationBase, nLeptons):
        b = equationBase
        nL = b.nLeptons
        self.assertEqual(b.indexToBinary(0), nL*"0")
        self.assertEqual(b.indexToBinary(1), (nL-1)*"0" + "1")
        self.assertEqual(b.indexToBinary((2**nL)-1), (nL)*"1")
        self.assertEqual(b.indexToBinary((2**nL)-2), (nL-1)*"1" + "0")

        self.assertEqual(b.measuredTypesToBinary(nL*"t"), nL*"0")
        self.assertEqual(b.measuredTypesToBinary((nL-1)*"t" + "l"), (nL-1)*"0" + "1")
        self.assertEqual(b.measuredTypesToBinary(nL*"l"), nL*"1")
        self.assertEqual(b.measuredTypesToBinary((nL-1)*"l" + "t"), (nL-1)*"1" + "0")

        self.assertEqual(b.trueTypesToBinary(nL*"r"), nL*"0")
        self.assertEqual(b.trueTypesToBinary((nL-1)*"r" + "f"), (nL-1)*"0" + "1")
        self.assertEqual(b.trueTypesToBinary(nL*"f"), nL*"1")
        self.assertEqual(b.trueTypesToBinary((nL-1)*"f" + "r"), (nL-1)*"1" + "0")

        for n in range(2**nL):
            self.assertEqual(b.binaryToIndex(b.indexToBinary(n)), n)
            binary = b.indexToBinary(n)
            self.assertEqual(b.measuredTypesToBinary(b.binaryToMeasuredTypes(binary)), binary)
            self.assertEqual(b.trueTypesToBinary(b.binaryToTrueTypes(binary)), binary)
        return


    def test_manyConversions(self):
        for nLeptons in range (1, 11):
            equationBase = EquationBase.EquationBase(nLeptons)
            self.conversions(equationBase, nLeptons)

    def test_checkTypes(self):
        b = EquationBase.EquationBase(2)
        self.assertTrue(b.checkTrueTypes("rr"))
        self.assertTrue(b.checkMeasuredTypes("ll"))
        self.assertFalse(b.checkMeasuredTypes("rr"))
        self.assertFalse(b.checkTrueTypes("ll"))

        b = EquationBase.EquationBase(3)
        self.assertTrue(b.checkTrueTypes("rfr"))
        self.assertTrue(b.checkMeasuredTypes("tlt"))
        self.assertFalse(b.checkMeasuredTypes("rfr"))
        self.assertFalse(b.checkTrueTypes("tlt"))

        b = EquationBase.EquationBase(4)
        self.assertTrue(b.checkTrueTypes("ffff"))
        self.assertTrue(b.checkMeasuredTypes("tttt"))
        self.assertFalse(b.checkMeasuredTypes("ffff"))
        self.assertFalse(b.checkTrueTypes("tttt"))

        self.assertFalse(b.checkTrueTypes(""))
        self.assertFalse(b.checkMeasuredTypes(""))

        b.nLeptons = None

    def test_otherConversions(self):
        import random
        X = random.uniform(0,1)
        while X == 0:
            X = random.uniform(0, 1)
        Y = random.randint(1, 100)

        X1 = EquationBase.EquationBase.efficiencyToFakeFactor(EquationBase.EquationBase.fakeFactorToEfficiency(X))
        Y1 = EquationBase.EquationBase.efficiencyToFakeFactor(EquationBase.EquationBase.fakeFactorToEfficiency(Y))
        self.assertAlmostEqual(X, X1)
        self.assertAlmostEqual(Y, Y1)
        X1 = EquationBase.EquationBase.fakeFactorToEfficiency(EquationBase.EquationBase.efficiencyToFakeFactor(X))
        Y1 = EquationBase.EquationBase.fakeFactorToEfficiency(EquationBase.EquationBase.efficiencyToFakeFactor(Y))
        self.assertAlmostEqual(X, X1)
        self.assertAlmostEqual(Y, Y1)

        return

    def tearDown(self):
        return

if __name__ == '__main__':
    unittest.main()
