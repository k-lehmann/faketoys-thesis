import unittest
from faketoys import FakeFactorEquation, MatrixEquation, EquationBase
from faketoys.stats import dists, derived_dists
import numpy as np

class TestFakeFactorEquation(unittest.TestCase):

    def setUp(self):
        return
    def tearDown(self):
        return

    def test_oneLepton(self):
        random = False
        nLeptons = 1
        e = FakeFactorEquation.FakeFactorEquation(nLeptons)

        self.assertTrue(e.setNEvents("f", "t", dists.Poisson(1, "Nft")))
        self.assertTrue(e.setNEvents("f", "l", dists.Poisson(2, "Nfl")))
        self.assertTrue(e.setNEvents("r", "t", dists.Poisson(3, "Nrt")))
        self.assertTrue(e.setNEvents("r", "l", dists.Poisson(4, "Nrl")))

        # wrong usage
        self.assertFalse(e.setNEvents("r", "l", 4))
        self.assertFalse(e.setNEvents("wrong", "input", dists.Poisson(4, "Nrl")))

        # Test that we can read everything we just set
        # Test that in those cases, also readNEvents returns the same result
        self.assertEqual(e.getNEvents ("f", "t").getTrueValue(), 1)
        self.assertEqual(e.readNEvents("f", "t").getTrueValue(), 1)
        self.assertEqual(e.getNEvents ("f", "l").getTrueValue(), 2)
        self.assertEqual(e.readNEvents("f", "l").getTrueValue(), 2)
        self.assertEqual(e.getNEvents ("r", "t").getTrueValue(), 3)
        self.assertEqual(e.readNEvents("r", "t").getTrueValue(), 3)
        self.assertEqual(e.getNEvents ("r", "l").getTrueValue(), 4)
        self.assertEqual(e.readNEvents("r", "l").getTrueValue(), 4)
        self.assertEqual(e.getNEvents ("r", "" ).getTrueValue(), 7)
        self.assertEqual(e.readNEvents("r", "" ), None)
        self.assertEqual(e.getNEvents ("f", "" ).getTrueValue(), 3)
        self.assertEqual(e.readNEvents("f", "" ), None)
        self.assertEqual(e.getNEvents ("", "t" ).getTrueValue(), 4)
        self.assertEqual(e.readNEvents("", "t" ), None)
        self.assertEqual(e.getNEvents ("", "l" ).getTrueValue(), 6)
        self.assertEqual(e.readNEvents("", "l" ), None)
        self.assertEqual(e.getNEvents ("", ""  ).getTrueValue(), 10)
        self.assertEqual(e.readNEvents("", ""  ), None)
        self.assertEqual(e.getNEvents ("wrong", "input"), None)
        self.assertEqual(e.readNEvents("wrong", "input"), None)

        self.assertFalse(e.setNEvents("r", "", 7))
        self.assertTrue(e.setNEvents( "r", "", dists.Poisson(7 , "r")))
        self.assertFalse(e.setNEvents("f", "", dists.Poisson(7 , "f")))
        self.assertTrue(e.setNEvents("f", "", dists.Poisson(7 , "f"), checkConsistency = False))
        self.assertTrue(e.setNEvents( "f", "", dists.Poisson(3 , "f")))
        self.assertTrue(e.setNEvents( "" , "", dists.Poisson(10, "overall")))
        self.assertFalse(e.setNEvents("" , "", dists.Poisson(7 , "overall")))
        return

    def checkNEvents(self, nLeptons):

        random = False

        e = FakeFactorEquation.FakeFactorEquation(nLeptons)
        d = e.dimension
        runningIndex = 0
        uncMatrix = 2
        for trueIndex in range(d):
            trueTypes = e.indexToTrueTypes(trueIndex)
            for measuredIndex in range(d):
                runningIndex = runningIndex + 1
                measuredTypes = e.indexToMeasuredTypes(measuredIndex)
                e.setNEvents(trueTypes, measuredTypes, dists.Gaussian(runningIndex, uncMatrix))

        for index in range(d):
            # Create a new distribution by summing the individual ones and compare to manual calculation of true value
            self.assertEqual(e.getNEvents(e.indexToTrueTypes(index), "").getTrueValue(), d*(d+1) / 2 + index*d*d)
            self.assertEqual(e.getNEvents("", e.indexToMeasuredTypes(index)).getTrueValue(), ((index+1)*d) + d*(d-1)*(d)/2)
            # Check that the uncertainty is estimated correctly
            self.assertEqual(e.getNEvents(e.indexToTrueTypes(index), "").getStddev(), np.sqrt(d*uncMatrix**2))
            self.assertEqual(e.getNEvents("", e.indexToMeasuredTypes(index)).getStddev(), np.sqrt(d*uncMatrix**2))
        # Check the same again, but for the overall yield
        self.assertEqual(e.getNEvents("", "").getTrueValue(), (d*d)*((d*d)+1)/2.)
        self.assertEqual(e.getNEvents("", "").getStddev(), np.sqrt(d*d*uncMatrix**2))


        # use a large and a small value for the uncertainty to see if the minimum of both uncertainties is used
        uncMeasuredVector = 3
        for uncTrueVector in [1.5, 123.3]:
            for index in range(d):
                trueTypes = e.indexToTrueTypes(index)
                nEventsTrueTypes = e.getNEvents(trueTypes, "").getTrueValue()
                self.assertFalse(e.setNEvents(trueTypes, "", dists.Gaussian(nEventsTrueTypes-1, 2)))
                self.assertTrue(e.setNEvents(trueTypes, "", dists.Gaussian(nEventsTrueTypes, uncTrueVector)))
                measuredTypes = e.indexToMeasuredTypes(index)
                nEventsMeasuredTypes = e.getNEvents("", measuredTypes).getTrueValue()
                self.assertFalse(e.setNEvents("", measuredTypes, dists.Gaussian(nEventsMeasuredTypes-1, 3)))
                if index == d-1:
                    # should fail (i.e. return None) because values are inconsistent
                    self.assertEqual(e.getNEvents("", ""), None)
                self.assertTrue(e.setNEvents("", measuredTypes, dists.Gaussian(nEventsMeasuredTypes, uncMeasuredVector)))

            # Now, do the check from above again for the overall yield. The true value should be the same,
            self.assertEqual(e.getNEvents("", "").getTrueValue(), (d*d)*((d*d)+1)/2.)
            # but the uncertainty smaller, because the minimum of the three possible distributions are used
            # All uncertainties are sqrt(quadratic sum of unc). The factors of d account for the number of terms
            smallestUnc = min(np.sqrt(d*uncTrueVector**2), np.sqrt(d*uncMeasuredVector**2), np.sqrt(d**2 * uncMatrix**2))
            self.assertEqual(e.getNEvents("", "").getStddev(), smallestUnc)

    def test_NEvents(self):
        for nLeptons in range(1, 4):
            self.checkNEvents(nLeptons)

    def test_fakeFactorInterface(self):
        e = FakeFactorEquation.FakeFactorEquation(2, name = "Fake factor equation")

        # Yields to initialize fake factor
        Nt  = dists.Poisson(11)
        Ntr = dists.Gaussian(2, 1)
        Nl  = dists.Poisson(40)
        Nlr = dists.Gaussian(10, 5)
        F0  = derived_dists.FakeFactor(Nt = Nt, Ntr = Ntr, Nl = Nl, Nlr = Nlr)
        Nl1 = dists.Poisson(50)
        F1  = derived_dists.FakeFactor(Nt = Nt, Ntr = Ntr, Nl = Nl1, Nlr = Nlr)

        # Nothing is set yet
        self.assertFalse(e.loadDistributions())

        self.assertFalse(e.setFakeFactor(-1, F0))
        self.assertTrue(e.setFakeFactor(0, F0))
        self.assertTrue(e.setFakeFactor(1, F1))
        # Even with both fake factors set, this doesn't work. Need
        # nominal distributions.
        self.assertFalse(e.loadDistributions())

        self.assertEqual(e.getFakeFactor(0), F0)
        self.assertEqual(e.getFakeFactor(1), F1)

        # This does not need to give meaningful results
        d = dists.Gaussian(20, 2)
        e.setNEvents('', 'tl', d)
        e.setNEvents('', 'lt', d)
        self.assertFalse(e.loadDistributions(printWarnings = True))
        e.setNEvents('', 'll', d)
        e.setNEvents('rr', 'tt', d)
        e.setNEvents('rr', 'tl', d)
        e.setNEvents('rr', 'lt', d)
        self.assertFalse(e.loadDistributions())
        e.setNEvents('rr', 'll', d)

        # Finally, it works
        self.assertTrue(e.loadDistributions())
        self.assertTrue(e.distributionsLoaded)
        self.assertTrue(hasattr(e, 'underlyingDistributions'))
        self.assertTrue(e.loadDistributions())

        # Unload distributions
        self.assertTrue(e.unloadDistributions())
        self.assertFalse(e.distributionsLoaded)
        self.assertFalse(hasattr(e, 'underlyingDistributions'))

        # Unload again. None are stored. Returns false.
        self.assertFalse(e.unloadDistributions())

        self.assertTrue(e.loadDistributions())

        # Setting a fake factor should set distributionsLoaded to false
        e.setFakeFactor(1, None)
        self.assertFalse(e.distributionsLoaded)

        # Test the quite artificial case that the number of FFs is
        # not equal to the number of leptons
        e.F = [F0]
        self.assertFalse(e.loadDistributions(printWarnings = True))
        e.F = [F0, None]

        # And it fails again if the FF1 is undefined
        self.assertFalse(e.loadDistributions(printWarnings = True))

        self.assertEqual(e.createNEventsFromMatrix("wrong", "input"), None)
        self.assertEqual(e.createNEventsFromTrueVector("wrong", "input"), None)
        self.assertEqual(e.createNEventsFromMeasuredVector("wrong", "input"), None)

        # Run some other methods without real testing
        e.printStoredValues()
        print(e.__repr__())

        return

    def test_fakeFactorPrecisionAnalytic(self):
        nLeptonRange = range(1,7)
        nIterations = 3
        random = False
        for nLeptons in nLeptonRange:
            sumRelativePrecision = 0
            for n in range(nIterations):
                equation = MatrixEquation.MatrixEquation(nLeptons)
                equation.initializeRandom()
                self.assertTrue(equation.initialized)

                ffe = FakeFactorEquation.FakeFactorEquation.fromMatrixEquation(equation)
                self.assertTrue(ffe.checkConsistency())
                self.assertTrue(ffe.loadDistributions())

                expectedFakeYield = equation.getNEvents("", nLeptons*"t") - equation.getNEvents(nLeptons*"r", nLeptons*"t")
                calculatedFakeYield = ffe.getTrueValue()

                self.assertTrue(EquationBase.EquationBase.valuesEqual(expectedFakeYield, calculatedFakeYield))

                relativePrecision = abs((expectedFakeYield - calculatedFakeYield)/expectedFakeYield)
                sumRelativePrecision = sumRelativePrecision + relativePrecision
                if (n == nIterations - 1):
                    print("Average relative precision (" + str(nLeptons) + " leptons) = " + str(sumRelativePrecision / nIterations))
        return

    def test_fakeFactorExample(self):

        Nt  = dists.Poisson(   50 + 75)
        Ntr = dists.Gaussian(      75, 10)
        Nl  = dists.Poisson(  200 + 50)
        Nlr = dists.Gaussian(      50, 40)
        F0  = derived_dists.FakeFactor(Nt = Nt, Ntr = Ntr, Nl = Nl, Nlr = Nlr)

        e = FakeFactorEquation.FakeFactorEquation(1)
        e.setFakeFactor(0, F0)

        # Set up mock FF equation with a fake yield of 2
        e.setNEvents("r", "t", dists.Gaussian(8, 3))
        e.setNEvents("" , "l", dists.Poisson(16))
        e.setNEvents("r", "l", dists.Gaussian(8, 3))

        self.assertTrue(e.checkConsistency())
        self.assertTrue(e.loadDistributions())
        self.assertEqual(e.getTrueValue(), 2)

        variedValues = [1.25, 2.75, 3.0, 1.0,
                        2.447213595499958,
                        1.5527864045000421,
                        1.6, 2.4,
                        1.8534703064066225,
                        2.17168692629778,
                        2.5,
                        1.6666666666666667]
        self.assertCountEqual(e.getListOfVariedValues(), variedValues)

        return


if __name__ == '__main__':
    unittest.main()
