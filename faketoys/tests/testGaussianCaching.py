import unittest

import numpy as np
from faketoys.stats import dists

class TestGaussian(unittest.TestCase):

    def setUp(self):
        return
    def tearDown(self):
        return

    def test_getRandom_caching(self):
        from faketoys.variations.models import SymmetricLinearVariation
        var = SymmetricLinearVariation(1)

        g = dists.Gaussian(1, 2)
        l = g.getRandom(nRandom = 20)
        self.assertEqual(len(l), 20)

        index = g.getSmallestUnusedCacheIndex()
        self.assertEqual(g.getRandom(cacheIndex = index, nRandom = 3), None)
        self.assertEqual(g.getRandom(cacheIndex = index, nRandom = "wrongArgType"), None)
        self.assertEqual(g.getRandom(systSources = [var], nRandom = 3), None)
        self.assertEqual(g.getRandom(statUnc = False, nRandom = 3), None)
        self.assertEqual(g.getRandom(systSources = [var]), None)
        self.assertEqual(g.getRandom(systSources = var), None)


        r1 = g.getRandom(cacheIndex = index)
        g1 = dists.Gaussian(20, 3)
        r2 = g.getRandom(cacheIndex = index)
        self.assertEqual(r1, r2)
        g2 = dists.Gaussian(200, 1)
        newIndex = g.getSmallestUnusedCacheIndex()
        self.assertEqual(newIndex, index + 1)
        r3 = g.getRandom(cacheIndex = newIndex)
        # It would be a crazy coincidence, if these are equal
        self.assertNotEqual(r1, r3)
        # Make sure there is no cross-talk between instances
        r4 = g2.getRandom(cacheIndex = newIndex)
        self.assertNotEqual(r3, r4)

        # Make sure that statUnc = False returns the correct value
        # even if cached before
        self.assertEqual(g.getRandom(cacheIndex = newIndex, statUnc = False), g.getTrueValue())
        self.assertEqual(g.getRandom(cacheIndex = newIndex, systSources = [], statUnc = False), g.getTrueValue())
        self.assertEqual(g.getRandom(systSources = [], statUnc = False), g.getTrueValue())
        self.assertEqual(g.getRandom(statUnc = False), g.getTrueValue())
        self.assertNotEqual(g.getRandom(statUnc = True), r3)
        self.assertEqual(g.getRandom(cacheIndex = newIndex, statUnc = True), r3)

        return


if __name__ == '__main__':
    unittest.main()
