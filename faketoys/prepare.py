#!/usr/bin/env python3

import xarray as xr
import numpy as np
import pickle
import xarray as xr
import argparse
from faketoys import log, utils

def main(args):

    conf = utils.importModuleFromConfig(args.config)
    log.INFO(f"Imported config from {args.config}")

    array = conf.getDataArray()
    ds = xr.Dataset({'equation': array})
    log.INFO("Created xarray Dataset of equations")

    utils.decorateDataset(ds, "prepare", args)

    outPath = utils.getOutputPath(conf.preparedOutputFile)
    with open(outPath, "wb") as f:
        pickle.dump(ds, f, protocol=4)
    log.INFO(f"Wrote equation Dataset to '{outPath}'")

if __name__ == '__main__':
    parser = utils.getArgParserPrepare()
    args = parser.parse_args()
    main(args)
