from enum import Enum
class EvaluationMode(Enum):
    '''Enum to steer behaviour of :py:func:`DistributionBase.evaluate`.
    '''
    UNDEFINED     = 0
    TRUEVALUE     = 10
    RANDOM        = 20
    EXPRESSION    = 30
    VARIATIONDICT = 40
    VARIATIONLIST = 41

from faketoys import log
from faketoys.variations.config import VariationConfig
import numpy as np
from uncertainties import ufloat

class DistributionBase(log.Logger):
    '''Base class for all distributions.

    Distributions are the fundamental building block for faketoys. They
    contain information about a distribution, like their PDF, a method
    to generate random numbers, the dependence on other distributions.

    When creating a new distribution, only a few methods need to be
    defined. These are
    :py:meth:`getTrueValue`,
    :py:meth:`getMean`,
    :py:meth:`getStddev`
    :py:meth:`_getRandom`,
    :py:meth:`__repr__`.
    See for example :py:class:`~faketoys.stats.dists.Gaussian`.

    The :py:class:`DistributionBase` provides an interface to calculate
    and retrieve values from the distribution in an interpretable way.
    It employs multiple concepts:

    **Random variables and caching**.
    Each inheriting class needs to define :py:meth:`_getRandom`. This
    method returns a random variable from the distribution. When
    retrieving random variables, the wrapper :py:meth:`getRandom` should
    be used. It allows to specify a `cacheIndex`. If this index is
    provided, a random variable is drawn and cached with the
    `cacheIndex` as identifier. When the method is called again, the
    same value is returned. If a new, different `cacheIndex` is
    provided, the old cached value is deleted and a new random value
    generated and cached.

    **Variations**.
    Each distribution can depend on other distributions. This way,
    systematic uncertainties can be modelled that affect multiple
    distributions at the same time. Suppose that we have two Poisson
    distributions `a`, `b`. They both depend on a third, Gaussian
    distribution `s` representing a systematic uncertainty. The
    distributions `a` and `b` behave oppositely to changes in `s` so
    that they are effectively anti-correlated to a degree. The exact
    dependence is encoded in a
    :py:class:`~faketoys.variations.config.VariationConfig`. Instances
    of :py:class:`~faketoys.variations.config.VariationConfig` are added
    to the distributions `a`, `b`. See the example below:

       >>> # Set seed for random number generator for reproducibility
       >>> from faketoys.stats.dist_base import DistributionBase
       >>> _ = DistributionBase.setRNG(seed=10)
       >>>
       >>> # Define distributions
       >>> from faketoys.stats.dists import Poisson, Gaussian
       >>> a = Poisson(100, name='a')
       >>> b = Poisson(50, name='b')
       >>> s = Gaussian(0, 2, name='s')
       >>>
       >>> # Define variation configs. AbsoluteVariation multiplies the
       >>> # random value from s with 10 or -20. It will be added to the
       >>> # random values of a and b, respectively.
       >>> from faketoys.variations.config import VariationConfig
       >>> from faketoys.variations.models import AbsoluteVariation
       >>> var_conf_a = VariationConfig(s, AbsoluteVariation(10), name='var_conf_a')
       >>> var_conf_b = VariationConfig(s, AbsoluteVariation(-20), name='var_conf_a')
       >>> _ = a.addVariationConfig(var_conf_a)
       >>> _ = b.addVariationConfig(var_conf_b)
       >>>
       >>> # Draw random variables w/o systs for illustration
       >>> print(a.getRandom(cacheIndex=1))
       124
       >>> print(b.getRandom(cacheIndex=1))
       58
       >>> print(s.getRandom(cacheIndex=1))
       -0.4971614588777817
       >>>
       >>> # Draw random variables with systs
       >>> print(a.getRandom(cacheIndex=1, systSources=[s]))
       119.02838541122219
       >>> print(b.getRandom(cacheIndex=1, systSources=[s]))
       67.94322917755564
       >>>
       >>> # Visualize how the variation acted on the random variables
       >>> a.printRandomTree(cacheIndex=1, systSources=[s])
       a (rand=119.02838541122219, raw=124)
         var_conf_a (rand=-4.9716145887778165)
           s (rand=raw=-0.4971614588777817)
       >>>
       >>> b.printRandomTree(cacheIndex=1, systSources=[s])
       b (rand=67.94322917755564, raw=58)
         var_conf_a (rand=9.943229177555633)
           s (rand=raw=-0.4971614588777817)

    In this example, the systematic uncertainty s fluctuated down. This
    caused `a` down fluctuation in distribution a, because a depends on
    `s` linearly after multiplying a factor of 10. The variable `b`,
    however, fluctuated up, because it is multiplied by -20. This way,
    an anti- correlation can be simulated.
    '''

    # logLevel = log.Level.DEBUG

    # Random number generator without seed
    _rng = np.random.default_rng()

    @staticmethod
    def setRNG(rng=None, seed=None):
        '''Set a random number generator for all derived classes.

        By default, DistributionBase provides a random number generator
        initialized without seed (i.e. initialized randomly). This
        function allows to overwrite the generator. If no arguments are
        passed, another random generator is initialized. If `rng` is
        provided, it will be used as new generator. If instead `seed` is
        given as argument, it will be used to initialize a new
        generator. Don't pass `rng` and `seed` at the same time.

        Args:
            rng (numpy.random.Generator): New random number generator.
            seed: Any seed that is accepted by numpy.
        Returns:
            Returns `True` if new generator was set. `False` in case of
            error.
        '''
        if rng != None and seed != None:
            DistributionBase.WARNING("Do not pass 'rng' and 'seed' to setRNG().")
            return False
        elif rng:
            if not isinstance(rng, np.random.Generator):
                DistributionBase.WARNING("'rng' needs to be a np.random.Generator.")
                return False
            DistributionBase.DEBUG("Setting new rng.")
            DistributionBase._rng = rng
        elif seed:
            DistributionBase.DEBUG(f"Initializing new rng with seed {seed}.")
            DistributionBase._rng = np.random.default_rng(seed)
        else:
            DistributionBase.DEBUG("Initializing new rng without seed.")
            DistributionBase._rng = np.random.default_rng()
        return True

    smallestUnusedCacheIndex = 0
    @staticmethod
    def getSmallestUnusedCacheIndex():
        '''See :py:func:`getAndUpdateSmallestUnusedCacheIndex`.'''
        return DistributionBase.smallestUnusedCacheIndex
    @staticmethod
    def updateSmallestUnusedCacheIndex(lastIndexUsed):
        '''See :py:func:`getAndUpdateSmallestUnusedCacheIndex`.'''
        DistributionBase.smallestUnusedCacheIndex = max(DistributionBase.smallestUnusedCacheIndex, lastIndexUsed + 1)
    @staticmethod
    def getAndUpdateSmallestUnusedCacheIndex():
        '''Get smallest unused cache index and update index.

        Cache indices are used to identify events and store information
        until the entire event is evaluated. To make sure that you get a
        new cache index every time, use this method. It returns the new
        cache index and updates the counter for next time. It is made
        out of the individual methods
        :py:func:`getSmallestUnusedCacheIndex` and
        :py:func:`updateSmallestUnusedCacheIndex`.

        Returns:
            int: New and unused cache index.
        '''
        retVal = DistributionBase.smallestUnusedCacheIndex
        DistributionBase.smallestUnusedCacheIndex = DistributionBase.smallestUnusedCacheIndex + 1
        return retVal

    def __init__(self, name=None):
        self.cacheIndex = None
        self.cachedValue = None
        self.name = None
        self.setName(name)
        self.variationConfigs = []
        self.expression = None
        return

    def setName(self, name):
        '''Set the name of this instance.

        This method should ideally only be used in the constructor.
        '''
        if hasattr(self, 'expression'):
            DistributionBase.WARNING("Note that the expression will not be renamed.")
        self.name = name
        return True

    def getName(self):
        return self.name

    ########## Methods to be overwritten ##########

    def getTrueValue(self):
        '''Returns the true value of the distribution.

        Needs to be implemented by inheriting class.'''
        raise NotImplementedError("Classes inheriting from DistributionBase must implement getTrueValue()")

    def getMean(self):
        '''Returns the mean of the distribution.

        Needs to be implemented by inheriting class.'''
        raise NotImplementedError("Classes inheriting from DistributionBase must implement getMean()")

    def getStddev(self):
        '''Returns the standard deviation of the distribution.

        Needs to be implemented by inheriting class.'''
        raise NotImplementedError("Classes inheriting from DistributionBase must implement getStddev()")

    def getVariationsForStatUnc(self, cacheIndex=None):
        '''Returns a list of variations inherent to this distribution.

        This could be an up and a down variation for example.
        Not essential convention: first entry is the up variation.

        Needs to be implemented by inheriting class.'''
        raise NotImplementedError("Classes inheriting from DistributionBase must implement getVariationsForStatUnc()")

    def _getRandom(self, nRandom=None):
        '''Returns the actual random variable(s).

        Needs to be implemented by inheriting class.'''
        raise NotImplementedError("Classes inheriting from DistributionBase must implement _getRandom()")
    def __repr__(self):
        raise NotImplementedError("Classes inheriting from DistributionBase must implement __repr__()")

    ########## Miscellaneous methods ##########

    def getNumberOfVariations(self):
        '''Get the number of built-in variations for this uncertainty.

        Returns:
            int: Number of variations
        '''
        return len(self.getVariationsForStatUnc())

    def __str__(self):
        if isinstance(self.name, str):
            return self.name
        else:
            return self.__repr__()

    def evaluate(self, mode, **kwargs):
        '''Evaluates the distributions.

        Args:
            mode (EvaluationMode): Specifies the way in which the
              distribution is evaluated. For each of the modes, the
              corresponding method is used. `UNDEFINED` returns None.
            **kwargs: Keyword arguments are forwarded to the methods
              specified by `mode`. Use for example with `cacheIndex`.

        Returns:
            Return value of the evaluation.
        '''
        if mode == EvaluationMode.UNDEFINED:
            DistributionBase.WARNING(f"Trying to evaluate {self} with undefined EvaluationMode. Returning None.")
            return None
        elif mode == EvaluationMode.TRUEVALUE:
            DistributionBase.DEBUG(f"Evaluating {self} with EvaluationMode TRUEVALUE")
            return self.getTrueValue(**kwargs)
        elif mode == EvaluationMode.RANDOM:
            DistributionBase.DEBUG(f"Evaluating {self} with EvaluationMode RANDOM")
            return self.getRandom(**kwargs)
        elif mode == EvaluationMode.EXPRESSION:
            DistributionBase.DEBUG(f"Evaluating {self} with EvaluationMode EXPRESSION")
            return self.getExpression(**kwargs)
        elif mode == EvaluationMode.VARIATIONDICT:
            DistributionBase.DEBUG(f"Evaluating {self} with EvaluationMode VARIATIONDICT")
            return self.getDictOfVariedValues(**kwargs)
        elif mode == EvaluationMode.VARIATIONLIST:
            DistributionBase.DEBUG(f"Evaluating {self} with EvaluationMode VARIATIONLIST")
            return self.getListOfVariedValues(**kwargs)
        DistributionBase.WARNING(f"EvaluationMode {mode} is not defined. Returning None.")
        return None

    @staticmethod
    def evaluateDistribution(distribution, mode, **kwargs):
        '''Like :py:func:`evaluate`, but takes distribution as argument.

        The advantage of this method is that it does not crash if the
        distribution is undefined.

        Args:
            distribution (DistributionBase): Distribution to evaluate.
            mode (EvaluationMode): See :py:func:`evaluate`.
            **kwargs: See :py:func:`evaluate`.

        Returns:
            If distribution is not a :py:class:`DistributionBase`,
            returns None. Otherwise return value of the evaluation.
        '''
        if distribution == None:
            return None
        return distribution.evaluate(mode, **kwargs)

    ########## Expressions with uncertainties ##########

    def _createExpression(self):
        '''Create an ufloat for this distribution.

        This method should only be called once and return an ufloat
        with the nominal value and the standard deviation. To give
        it a nice name, the inheriting class can overwrite it.
        The returned ufloat can be accessed with
        :py:func:`getExpressionStatOnly`.'''
        return ufloat(self.getMean(), self.getStddev(), self.__str__())

    def getExpressionStatOnly(self):
        '''Returns ufloat for this distribution.

        Same as :py:func:`getExpression` with argument `True`.'''
        if self.expression == None:
            self.expression = self._createExpression()
        return self.expression

    def getExpression(self, statOnly=False):
        '''Returns an ufloat with stat+syst uncertainties.

        The ufloat for this distribution includes statistical and
        systematic uncertainties. If statOnly is True, it does the
        method is equivalent to :py:func:`getExpressionStatOnly`.

        Only works for the true value at the moment. Would be nice to
        extend to use `cacheIndex`.

        Args:
            statOnly (bool): Whether to use only statistical
              uncertainties or also systematic ones.
        Returns:
            ufloat: expression describing distribution
        '''
        DistributionBase.DEBUG("Entering getExpression()")
        expression = self.getExpressionStatOnly()
        DistributionBase.DEBUG(f"Stat-only expression for {self} is {expression}")
        if statOnly == True:
            DistributionBase.DEBUG(f"Returning expression {expression} for {self}")
            return expression
        for variationConfig in self.variationConfigs:
            expression = expression + variationConfig.getExpression(expression=self.getExpressionStatOnly())
        DistributionBase.DEBUG(f"Returning expression {expression} for {self}")
        return expression

    ########## Variations (up and down) ##########
    # Naming:
    #   - varied value refers to the true value with a systematic
    #     variation added on top
    #   - variation refers only to the systematic variation
    #   - statUnc refers to the uncertainty of the current distribution
    #   - systSource refers to other distributions that the current one
    #     can depend on

    def findAllSources(self, statOnly=False):
        '''Searches recursively for all sources of uncertainty.

        Args:
            statOnly (bool): If true, only the statistical uncertainty
              of this distribution is considered. Otherwise all sources
              of uncertainty are considered.
        Returns:
            list: Distributions that are direct or indirect (i.e.
            through multiple instances of variationConfigs)
            uncertainties.
        '''
        sources = [self]
        if statOnly:
            return sources
        for variationConfig in self.variationConfigs:
            sources.extend(variationConfig.findAllSources())
        retVal = list(dict.fromkeys(sources))
        DistributionBase.DEBUG(f"(findAllSources()) Returning {retVal}")
        return retVal

    def findStatSources(self):
        '''Same as :py:func:`findAllSources` with argument True.'''
        return [self]

    def getCachedValue(self, cacheIndex, printWarning=True):
        '''Resolve cacheIndex. If it doesn't match, return true value.

        When random variables are generated, a cacheIndex can be used to
        save the variables temporarily. This can be useful if other
        methods are executed afterwards and need access to the generated
        random variable. The cacheIndex is an integer that identifies
        one set of generated variables. When a new cacheIndex is passed,
        the old variables will be forgotten. If you use an old
        cacheIndex, a new set of random variables will be generated.

        Args:
            cacheIndex (int): Cache index identifying an event.
            printWarning (bool): Whether a warning is printed when the
              `cacheIndex` is not the current one.
        Returns:
            float: If `cacheIndex` describes the currently cached value,
            return cached value. Otherwise return true value.
        '''
        if cacheIndex == None:
            return self.getTrueValue()
        if cacheIndex == self.cacheIndex:
            return self.cachedValue
        message = f"Trying to get cached event for cacheIndex {cacheIndex}, but this event is not cached. The current cache index is {self.cacheIndex}. Returning true value instead."
        DistributionBase.DEBUG(message)
        if printWarning:
            DistributionBase.WARNING(message)
        return self.getTrueValue()

    def getVariedValuesForStatUnc(self, cacheIndex=None):
        '''Get a list of varied values.

        Resolve `cacheIndex` with :py:func:`getCachedValue`. Use the
        return value to vary the current distribution. Return the
        varied values (typically up- and down variation).

        This method effectively just adds the cached (i.e. random) or
        true value to the output of :py:func:`getVariationsForStatUnc`.

        Args:
            cacheIndex (int): Cache index identifying an event.
        Returns:
            list: Varied values of random or true variable.
        '''
        nominalValue = self.getCachedValue(cacheIndex)
        variedValues = np.asarray([nominalValue for x in range(self.getNumberOfVariations())])
        variedValues = (variedValues + np.asarray(self.getVariationsForStatUnc(cacheIndex=cacheIndex))).tolist()
        DistributionBase.DEBUG(f"Statistically varied values of {self} are {variedValues}")
        return variedValues

    def getVariationsForOneSystSource(self, systSource, cacheIndex=None):
        """Calculate impact of one systematic variation on distribution.

        Loop over all systematic variations (encoded as variationConfigs
        of the systematic source and find the effect on distribution.
        Returns a list of variations on the distribution for each
        configured variation of the underlying distribution (typically
        one for "up", one for "down").
        """
        DistributionBase.DEBUG(f"Entering getVariationsForOneSystSource({systSource})")
        sumOfVariations = np.asarray([0 for x in range(systSource.getNumberOfVariations())])
        nominalValue = self.getCachedValue(cacheIndex, printWarning=False)
        for variationConfig in self.variationConfigs:
            DistributionBase.DEBUG(f"Getting variations for {systSource} from {variationConfig}")
            variations = np.asarray(variationConfig.getVariations(systSource=systSource, cacheIndex=cacheIndex, nominalValue=nominalValue))
            DistributionBase.DEBUG(f"Got variations: {variations.tolist()}")
            if len(sumOfVariations) != len(variations):
                DistributionBase.ERROR("Internal error, length of lists does not match")
            sumOfVariations = sumOfVariations + variations
        if systSource == self:
            DistributionBase.DEBUG(f'Current source is this distribution ({self}). Varying "stat" yields.')
            sumOfVariations = sumOfVariations + self.getVariationsForStatUnc(cacheIndex=cacheIndex)
        sumOfVariations = sumOfVariations.tolist()
        DistributionBase.DEBUG(f"The nominal value {nominalValue} (true value or random value with stat only) of {self} is affected by systSource {systSource} by {sumOfVariations}")
        DistributionBase.DEBUG(f"Returning from getVariationsForOneSystSource({systSource})")
        return sumOfVariations

    def getVariedValuesForOneSystSource(self, systSource, cacheIndex=None):
        """Same as getVariationsForOneSystSource with nominalValue added"""
        DistributionBase.DEBUG("Entering getVariedValuesForOneSystSource()")
        nominalValue = self.getCachedValue(cacheIndex, printWarning=False)
        nominalValues = np.asarray([nominalValue for x in range(systSource.getNumberOfVariations())])
        variedValues = (nominalValues + np.asarray(self.getVariationsForOneSystSource(systSource, cacheIndex=cacheIndex))).tolist()
        DistributionBase.DEBUG(f"Systematically varied values of {self} are {variedValues}")
        return variedValues

    def getDictOfVariedValues(self, systSources=None, statUnc=True, cacheIndex=None):
        '''Get varied values of this varible for systematic sources.

        Takes a list of `systSources` as input and returns a dictionary
        of them. For each entry, it holds a list of values of this
        current distribution, which are calculated using a varied value
        of the systematic source. If the dict key is this distribution,
        the variation corresponds to the statistical uncertainty.

        Args:
            systSources (DistributionBase): Considered sources of
              systematic uncertainty.
            statUnc (bool): Add the statistical uncertainty to the list
              of `systSources`, if it is not already in `systSources`.
            cacheIndex (int): Vary the uncertainties of the current set
              of random variables identified by `cacheIndex`.

        Returns:
            dict: Varied values for systematic uncertainties
        '''
        DistributionBase.DEBUG("Entering getDictOfVariedValues()")
        if systSources == None:
            allSources = []
        else:
            allSources = systSources.copy()
        # stat
        if statUnc == True:
            DistributionBase.DEBUG("Adding statistical uncertainties to sources of uncertainties")
            if not self in allSources:
                # Put statistical uncertainty first. Not really necessary, but
                # it makes unittesting easier.
                allSources.insert(0, self)
        elif self in allSources:
            DistributionBase.WARNING(f"(getDictOfVariedValues()) The statistical variation of {self} is in systSources, but the argument statUnc is set to false. Because it's in the list, the statistical uncertainty will be considered anyway. Remove it from the list to ignore it.")
        if allSources == []:
            DistributionBase.WARNING("Neither statistical uncertainty nor systematic sources requested. Returning empty dictionary.")
            return {}

        # sys
        # converting into dict removes duplicate entries and preserves order
        retDict = {}
        for source in list(dict.fromkeys(allSources)):
            DistributionBase.DEBUG(f"(getDictOfVariedValues()) Considering source {source}")
            retDict[source] = self.getVariedValuesForOneSystSource(cacheIndex=cacheIndex, systSource=source)
        DistributionBase.DEBUG(f"Returning full dictionary {retDict}")
        return retDict

    def getListOfVariedValues(self, systSources=None, statUnc=True, cacheIndex=None):
        """Same as :py:meth:`getDictOfVariedValues`, but returns a list."""
        dictionary = self.getDictOfVariedValues(systSources=systSources, statUnc=statUnc, cacheIndex=cacheIndex)
        l = []
        for key in dictionary:
            l.extend(dictionary[key])
        return l

    ########## Random variables ##########

    def printRandomTree(self, cacheIndex=None, systSources=[], level=0, depth=100):
        '''Print the values of all random variables recursively.

        Args:
            cacheIndex (int): List random values of most recent set of
              random variables identified by `cacheIndex`. If no
              `cacheIndex` is provided, the true values are printed.
            systSources (list): Systematic sources. Equivalent handling
              as in :py:meth:`getRandom` (unused without cacheIndex).
            level (int): Specifies the initial indentation.
            depth (int): Maximum recursion depth.
        '''
        DistributionBase.DEBUG("Entering printRandomTree()")
        s = level*"  "
        if cacheIndex==None:
            print(s + f"{self} (true value {self.getTrueValue()})")
            return

        rand = self.getRandom(cacheIndex=cacheIndex, systSources=systSources)
        raw = self.getCachedValue(cacheIndex)
        if raw == rand:
            print(s + f"{self} (rand=raw={rand})")
        else:
            print(s + f"{self} (rand={rand}, raw={raw})")

        if level > depth:
            return

        for variationConfig in self.getVariationConfigs():
            confSources = variationConfig.findAllSources()
            if not any(s in systSources for s in confSources):
                continue
            variationConfig.printRandomTree(cacheIndex=cacheIndex, systSources=systSources, level=level+1, depth=depth)

    def _getRandom_resolveSystSources(self, cacheIndex=None, systSources=None, statUnc=True, expression=False, rng=None):
        """Returns sum of all syst variations while drawing random vars.

        If systematic variations depend on the value of the random
        variable (e.g. a 10% uncertainty), the random value with
        statistical uncertainties is used as a basis for the percentage.
        This can be deactivated by passing statUnc=False. In this case,
        the true value of the distribution is used. Similarly, the true
        is used if cacheIndex is None.
        """
        DistributionBase.DEBUG("Entering _getRandom_resolveSystSources()")
        sumOfSysVariations = 0
        if not isinstance(systSources, list):
            DistributionBase.ERROR(f"'systSources' has to be a list. Returning None.")
            return None
        for source in systSources:
            if not isinstance(source, DistributionBase):
                DistributionBase.ERROR(f"'systSources' has to be a list of instances of DistributionBase. Returning None.")
                return None
        if statUnc:
            nominalValue = self.getCachedValue(cacheIndex)
        else:
            DistributionBase.DEBUG("Setting nominalValue = trueValue because no statistical variation of {self} is requested.")
            nominalValue = self.getTrueValue()
        for variationConfig in self.getVariationConfigs():
            sysVariation = variationConfig.drawVariation(cacheIndex=cacheIndex, systSources=systSources, nominalValue=nominalValue, expression=expression, rng=rng)
            DistributionBase.DEBUG(f"{variationConfig} causes a variation of {sysVariation} to {self}.")
            sumOfSysVariations = sumOfSysVariations + sysVariation
        DistributionBase.DEBUG(f"Returning systematic variation {sumOfSysVariations}")
        return sumOfSysVariations

    def getRandom(self, nRandom=None, cacheIndex=None, systSources=None, statUnc=True, expression=False, rng=None):
        '''Draw a random variable from this distribution.

        Args:
            nRandom (int): Number of random variables to draw.
              ``nRandom`` cannot be used together with any other
              arguments.
            cacheIndex (int): If a random variable with ``cacheIndex``
              is cached, return it. Otherwise generate a new random
              variable and cache it with ``cacheIndex`` for the future.
              Note that cached variables will only be stored until a new
              ``cacheIndex`` is used in this method.
            systSources (DistributionBase): List of systematic sources
              of uncertainty to consider.
            statUnc (bool): If True, adds statistical uncertainty to
              ``systSources``, if it is not already included.
            expression (bool): If True, this method returns an instance
              of ``uncertainties.ufloat``. Use together with
              ``cacheIndex``.
            rng (``numpy.random.Generator``): If provided, this random
              number generator will be used. Otherwise, the random
              number generator of :py:class:`DistributionBase` is used,
              which can be initialized via :py:meth:`setRNG`.

        Returns:
            Random variable or list of random variables depending on the
            argument ``nRandom``.
        '''

        # Resolve rng
        if rng == None:
            rng = self._rng

        # Resolve nRandom
        DistributionBase.DEBUG("Entering getRandom()")
        if (not nRandom == None):
            if not isinstance(nRandom, (int, np.int64)):
                DistributionBase.WARNING(f"getRandom() nRandom is of type {type(nRandom)}, but 'int' is expected. Continuing to run anyway.")
            if not (cacheIndex == None and systSources == None and statUnc == True and expression == False):
                DistributionBase.ERROR("getRandom(): Cannot use nRandom together with other arguments. Only one value can be cached. VariationConfigs require caching. Returning None.")
                return None
            return self._getRandom(nRandom=nRandom, rng=rng)
        if statUnc == False and not systSources == None and self in systSources:
            DistributionBase.WARNING(f"(getRandom()) The statistical variation of {self} is in systSources, but the argument statUnc is set to false. Because it's in the list, the statistical uncertainty will be considered anyway. Remove it from the list to ignore it.")
            statUnc = True

        # Resolve statUnc
        if statUnc == False:
            rand_statOnly = self.getTrueValue()
            DistributionBase.DEBUG(f"No statistical uncertainty requested. The value of {self} is {rand_statOnly} (no sys yet)")
        # Resolve caching (only needed if statUnc requested)
        elif cacheIndex == None:
            DistributionBase.DEBUG("Caching not used. Drawing new value with stat uncertainty.")
            rand_statOnly = self._getRandom(rng=rng)
            if expression:
                DistributionBase.WARNING("You are retrieving an expression, but don't use cacheIndex. This could cause wrong results.")
            DistributionBase.DEBUG(f"The random value of {self} is {rand_statOnly} (stat only)")
        elif self.cacheIndex == cacheIndex:
            DistributionBase.DEBUG(f"Cached value {self.cachedValue} found for cacheIndex {cacheIndex}.")
            rand_statOnly = self.cachedValue
        else:
            DistributionBase.DEBUG(f"No cached value found for {self}. Will produce and cache it with cacheIndex {cacheIndex}.")
            DistributionBase.updateSmallestUnusedCacheIndex(cacheIndex)
            self.cacheIndex = cacheIndex
            rand_statOnly = self._getRandom(rng=rng)
            self.cachedValue = rand_statOnly
            DistributionBase.DEBUG(f"Caching {self.cachedValue}")

        # Resolve expression
        if expression:
            stddev = 0
            if statUnc:
                stddev = self.getStddev(cacheIndex=cacheIndex)
            DistributionBase.DEBUG(f"Converting {rand_statOnly} +/- {stddev} into ufloat")
            rand_statOnly = ufloat(rand_statOnly, stddev, self.__str__())
            DistributionBase.DEBUG(f"Ufloat is {rand_statOnly}")

        # Resolve systSources
        if systSources == None:
            DistributionBase.DEBUG(f"No systSources requested for {self}. Returning previously produced value {rand_statOnly}")
            return rand_statOnly
        sysVariation = self._getRandom_resolveSystSources(cacheIndex=cacheIndex, systSources=systSources, statUnc=statUnc, expression=expression, rng=rng)
        if sysVariation == None:
            # Note that we even cache the stat value in this case
            return None
        retval = rand_statOnly + sysVariation
        DistributionBase.DEBUG(f"Returning {retval}. Systematics cause a variation of {sysVariation} on top of the value {rand_statOnly}.")
        return retval


    ########## Variation configs ##########

    def printVariationTree(self, statUnc=True, level=0, depth=100):
        '''Print the systematic variations of distributions recursively.

        Args:
            statUnc (bool): If True, print variation of this
              distribution explicitly.
            level (int): Specifies the initial indentation.
            depth (int): Maximum recursion depth.
        '''
        DistributionBase.DEBUG("Entering printVariationTree()")
        s = level*" "
        if statUnc == True:
            print(s + f"{self} (nom. val. {self.getTrueValue()})")
            print(s + "{:10.2f} stat up".format(self.getStddev()))
            print(s + "{:10.2f} stat down".format(-self.getStddev()))

        if level > depth:
            return

        for variationConfig in self.getVariationConfigs():
            variationConfig.printVariationTree(level=level+1, depth=depth)

    def addVariationConfig(self, variationConfig):
        '''Add a variation config to this distribution.

        Defines how this distribution depends on a systematic variation.
        The variation config holds all information.

        Args:
            variationConfig (VariationConfig): Variation config to be
              added.
        '''
        if not isinstance(variationConfig, VariationConfig):
            DistributionBase.ERROR("Argument variationConfig has to be in instance of VariationConfig")
            return False
        if variationConfig in self.variationConfigs:
            DistributionBase.WARNING(f"{variationConfig} is already in the variation configs for {self}. Refusing to add.")
            return False
        DistributionBase.DEBUG(f"Adding {variationConfig} to {self}")
        self.variationConfigs.append(variationConfig)
        return True

    def addVariationConfigs(self, variationConfigs):
        '''Same as :py:meth:`addVariationConfig`, but takes a list.'''

        if not isinstance(variationConfigs, list):
            DistributionBase.ERROR("Argument variationConfigs has to be a list")
            return False
        success = True
        for variationConfig in variationConfigs:
            success = self.addVariationConfig(variationConfig) and success
        return success

    def getAffectedVariationConfigs(self, systSources=None):
        '''Get variation configs that are affected by systSources.

        Args:
            systSources (list): List of :py:class:`DistributionBase`.

        Returns:
            list: variation configs that are affected by `systSources`.
        '''
        if not isinstance(systSources, list):
            DistributionBase.ERROR(f"'systSources' has to be a list. Returning None.")
            return None
        for variation in systSources:
            if not isinstance(variation, DistributionBase):
                DistributionBase.ERROR(f"'systSources' has to be a list of instances of DistributionBase. Returning an empty list.")
                return []

        variationConfigs = []
        for variationConfig in self.getVariationConfigs():
            allSources = variationConfig.findAllSources()
            affected = any([x in allSources for x in systSources])
            if affected:
                variationConfigs.append(variationConfig)
        return variationConfigs

    def removeVariationConfig(self, variationConfig):
        '''Remove `variationConfig` from this distribution.'''
        if not isinstance(variationConfig, VariationConfig):
            DistributionBase.ERROR("Argument variationConfig has to be in instance of VariationConfig")
            return False
        if variationConfig in self.variationConfigs:
            self.variationConfigs.remove(variationConfig)
            return True
        return False
    def getVariationConfigs(self):
        '''Get the list of variation configs of this distribution.'''
        return self.variationConfigs
