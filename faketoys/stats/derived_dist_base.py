from faketoys.stats import dist_base
import numpy as np

class DerivedDistributionBase(dist_base.DistributionBase):
    """"""

    def __init__(self, name = None):
        super().__init__(name = name)
        return

    def hasUnderlyingDistributions(self):
        return hasattr(self, 'underlyingDistributions')

    def unsetUnderlyingDistributions(self):
        if self.hasUnderlyingDistributions():
            del self.underlyingDistributions
        return

    def setUnderlyingDistributions(self, underlyingDistributions):
        # Call in constructor of inheriting class
        self.underlyingDistributions = underlyingDistributions.copy()
        return

    @staticmethod
    def calculateValue():
        """Not implemented for DerivedDistributionBase"""
        raise NotImplementedError("Classes inheriting from DerivedDistributionBase must implement calculateValue()")
    def _getRandom():
        """Not implemented for DerivedDistributionBase"""
        raise NotImplementedError("The method '_getRandom()' of DerivedDistributionBase should not be used. It does not make sense, because a derived distribution does not have a statistical uncertainty itself.")
    def getNumberOfVariations(self):
        """Not implemented for DerivedDistributionBase"""
        raise NotImplementedError("The method 'getNumberOfVariations()' of DerivedDistributionBase should not be used. Derived distributions don't have their own stat variation.")
    def getVariationsForStatUnc(self):
        """Not implemented for DerivedDistributionBase"""
        raise NotImplementedError("The method 'getVariationsForStatUnc()' of DerivedDistributionBase should not be used. Derived distributions don't have their own stat variation.")
    def getVariedValuesForStatUnc(self):
        """Not implemented for DerivedDistributionBase"""
        raise NotImplementedError("The method 'getVariedValuesForStatUnc()' of DerivedDistributionBase should not be used. Derived distributions don't have their own stat variation.")
    def getVariationsForOneSystSource(self):
        """Not implemented for DerivedDistributionBase"""
        raise NotImplementedError("The method 'getVariationsForOneSystSource()' of DerivedDistributionBase should not be used. Please use getVariedValuesForOneSystSource instead. This method would be quite computing-intensive, because the method getRandom would have to be called (and all its arguments would be required).")

    def getTrueValue(self):
        if not self.hasUnderlyingDistributions():
            DerivedDistributionBase.WARNING("(getTrueValue) No underlying distributions set. Returning 0.")
            return 0
        d = dict([(key, dist_base.DistributionBase.evaluateDistribution(dist, dist_base.EvaluationMode.TRUEVALUE)) for (key, dist) in self.underlyingDistributions.items()])
        return self.calculateValue(d)
    def _createExpression(self):
        raise RuntimeError("The method '_createExpression()' of the FakeFactor class should not be used. The definition of the base class does not make sense, because the expression is derived from other expressions.")
    def getExpressionStatOnly(self):
        # StatOnly meaning the statistical variations of underlying distributions
        return self.getExpression(statOnly = True)
    def getExpression(self, statOnly = False):
        if not self.hasUnderlyingDistributions():
            DerivedDistributionBase.WARNING("(getExpression) No underlying distributions set. Returning 0.")
            return 0
        d = dict([(key, dist.getExpression(statOnly = statOnly)) for (key, dist) in self.underlyingDistributions.items()])
        return self.calculateValue(d)

    def getVariedValuesForOneSystSource(self, systSource, cacheIndex=None):
        """Propagates varied values of underlying variables.

        Returns a list of varied values of this distribution with one
        entry for each variation of the systematic source (typically
        "up" and "down").
        """
        DerivedDistributionBase.DEBUG(f"Entering getVariationsForOneSystSource({systSource})")
        dictOfVariedValues = dict([(key, value.getVariedValuesForOneSystSource(systSource=systSource, cacheIndex=cacheIndex)) for (key, value) in self.underlyingDistributions.items()])
        DerivedDistributionBase.DEBUG(f"set of varied values {dictOfVariedValues}")
        variedValuesOfThisDistribution = []
        for variationIndex in range(systSource.getNumberOfVariations()):
            DerivedDistributionBase.DEBUG(f"Looping over variation index {variationIndex}")
            variedValues = dict([(key, value[variationIndex]) for (key, value) in dictOfVariedValues.items()])
            DerivedDistributionBase.DEBUG(f"The varied values are {variedValues}")
            variedValueOfThisDistribution = self.calculateValue(variedValues)
            DerivedDistributionBase.DEBUG(f"The varied value of {self} is {variedValueOfThisDistribution}")
            variedValuesOfThisDistribution.append(variedValueOfThisDistribution)
        DerivedDistributionBase.DEBUG(f"getVariedValuesForOneSystSource({systSource}, {cacheIndex}) is returning the varied values {variedValuesOfThisDistribution}")
        return variedValuesOfThisDistribution

    def getDictOfVariedValues(self, systSources=None, statUnc=True, cacheIndex=None):
        if not self.hasUnderlyingDistributions():
            DerivedDistributionBase.WARNING("(getDictOfVariedValues) No underlying distributions set. Returning empty dict.")
            return {}
        DerivedDistributionBase.DEBUG("Entering getDictOfVariedValues()")
        if systSources == None:
            allSources = []
        else:
            allSources = systSources.copy()
        if statUnc == True:
            allSources.extend(self.findAllSources(statOnly=True))
        retDict = {}
        for source in list(dict.fromkeys(allSources)):
            DerivedDistributionBase.DEBUG(f"(FakeFactor.getDictOfVariedValues()) Considering source {source}")
            retDict[source] = self.getVariedValuesForOneSystSource(systSource=source, cacheIndex=cacheIndex)
        DerivedDistributionBase.DEBUG(f"Returning full dictionary {retDict}")
        return retDict

    def findAllSources(self, statOnly=False):
        if not self.hasUnderlyingDistributions():
            DerivedDistributionBase.WARNING("(findAllSources) No underlying distributions set. Returning empty list.")
            return []
        sources = []
        for distribution in self.underlyingDistributions.values():
            sources.extend(distribution.findAllSources(statOnly=statOnly))
        retVal = list(dict.fromkeys(sources))
        DerivedDistributionBase.DEBUG(f"(findAllSources()) Returning {retVal}")
        return retVal

    def printRandomTree(self, level=0, depth=100, cacheIndex=None):
        DerivedDistributionBase.DEBUG("Entering printRandomTree()")
        if not self.hasUnderlyingDistributions():
            return
        s = level*"  "
        if cacheIndex == None:
            print(s + f"{self} (true value {self.getTrueValue()})")
        else:
            print(s + f"{self} (random value {self.getRandom(cacheIndex=cacheIndex)})")
        for uD in self.underlyingDistributions.values():
            if level >= depth:
                return
            uD.printRandomTree(level=level+1, depth=depth, cacheIndex=cacheIndex)

    def getRandom(self, nRandom=None, cacheIndex=None, systSources=None, statUnc=True, expression=False, rng=None):
        DerivedDistributionBase.DEBUG(f"Entering DerivedDistributionBase.getRandom(nRandom={nRandom}, cacheIndex={cacheIndex}, systSources={systSources}, statUnc={statUnc})")
        if not self.hasUnderlyingDistributions():
            DerivedDistributionBase.WARNING("(getRandom) No underlying distributions set. Returning 0.")
            return 0
        # Extra handling for nRandom != None
        if nRandom != None:
            for dist in self.underlyingDistributions.values():
                rand = dist.getRandom(nRandom=nRandom, cacheIndex=cacheIndex, systSources=systSources, statUnc=statUnc, expression=expression, rng=rng)
                if isinstance(rand, list):
                    break
                if rand == None:
                    DerivedDistributionBase.ERROR("Some error occured in DistributionBase.getRandom(). Returning None.")
                    return None
                # only test this once
                break
            DerivedDistributionBase.DEBUG("Arguments seem to be correct")
            DerivedDistributionBase.DEBUG("Retrieving the random values of all underlying distributions")
            dictOfRandomValues = dict([(key, value.getRandom(nRandom=nRandom, cacheIndex=cacheIndex, systSources=systSources, statUnc=statUnc, expression=expression, rng=rng)) for (key, value) in self.underlyingDistributions.items()])
            DerivedDistributionBase.DEBUG("Deriving distribution from generated variables")
            listOfRandomValues = []
            for i in range(nRandom):
                DerivedDistributionBase.DEBUG(f"Index {i}/{nRandom}")
                inputDict = dict([(key, value[i]) for (key, value) in dictOfRandomValues.items()])
                DerivedDistributionBase.DEBUG(f"Input variables {inputDict}")
                value = self.calculateValue(inputDict)
                DerivedDistributionBase.DEBUG(f"Derived value comes out to {value}")
                listOfRandomValues.append(value)
            DerivedDistributionBase.DEBUG(f"Returning {listOfRandomValues}")
            return listOfRandomValues

        # Can use implementation in underlying distributions
        DerivedDistributionBase.DEBUG("Getting all underlying random variables")
        d = dict([(key, dist.getRandom(cacheIndex=cacheIndex, systSources=systSources, statUnc=statUnc, expression=expression, rng=rng)) for (key, dist) in self.underlyingDistributions.items()])
        DerivedDistributionBase.DEBUG(f"Calculating return value from {d}")
        retVal = self.calculateValue(d)
        DerivedDistributionBase.DEBUG(f"Returning {retVal}")
        return retVal
