#!/usr/bin/env python3

import pickle
import xarray as xr
import matplotlib.pyplot as plt
import numpy as np
import argparse
from faketoys import utils, plot, log

############################################################
# intermediate development that can probably be deleted

from scipy.optimize import curve_fit

def acosModel(x, a, b, c):
    '''A 3-parameter model for the shape of the p-value distribution.'''
    # s-shape curve using rescaled acos and added to a linear function
    s = (c / (np.pi/2.-1)) * (np.arccos(2*x-1) + 2*x - np.pi/2. - 1)
    # Add linear function to s-shape from before
    f = a * (1 + b * (x - 0.5) + s)
    return f

def acosModel_estimateParam(weights):
    '''Estimates initial parameters for acosModel.'''
    a = sum(weights) / len(weights)
    b = -0.1
    c = (weights[0] - weights[-1]) / a
    return (a,b,c)

def acosModel_paramNames():
    '''Assigns names to the three parameters'''
    return ["a", "b", "c"]

def binsToX(bins):
    '''Takes bin edges and returns bin centers.'''
    return np.asarray([(bins[i]+bins[i+1])/2. for i in range(len(bins)-1)])

def fitPValue(func, func_estimateParam, func_paramNames, dataset, plot=False, histogramName="h_pValue"):
    '''Fit func to histogram in dataset and return post-fit parameters.

    If the dataset has 0 dimensions, the simple parameters are returned.
    If it has dimensions, an dataarray is returned with a new dimension
    for the post-fit parameters.
    '''
    dataarray = dataset[histogramName]
    if len(dataset.dims) == 0:
        weights = dataarray.item()[0]
        bins = dataarray.item()[1]
        return fit(func, func_estimateParam, bins, weights, plot=plot)
    else:
        bins = xr.apply_ufunc(extractBins, dataarray, vectorize=True, output_dtypes=[np.dtype('object')])
        weights = xr.apply_ufunc(extractWeights, dataarray, vectorize=True, output_dtypes=[np.dtype('object')])
        pOpt = xr.apply_ufunc(fit, func, func_estimateParam, bins, weights, kwargs={"plot": plot, "returnParams": True, "returnCov": False}, vectorize=True, output_core_dims=[["pOpt"]])
        pOpt["pOpt"] = func_paramNames()
        return pOpt

def fit(func, func_estimateParam, bins, weights, plot=False, returnParams=True, returnCov=False):
    '''Fit histogram to a custom function.

    func is a function that takes x-values and parameters as arguments
    and returns a y-value. func_estimateParam takes histogram weights
    and estimates the initial set of parameters for func. bins and
    weights are are bin edges and weights of the histogram to be fitted.
    If plot is true, then the histogram and the fitted function will be
    plotted. Returns the post-fit parameters and the covariance matrix.
    '''
    p0 = func_estimateParam(weights)
    x = binsToX(bins)
    pOpt, pCov = curve_fit(func, x, weights, p0)
    if plot:
        _ = plt.hist(bins[0:-1], weights=weights, bins=bins)
        plt.plot(x, func(x, *pOpt), 'r-')
    if returnParams and returnCov:
        return pOpt, pCov
    elif returnParams:
        return pOpt
    elif returnCov:
        return pCov
    return

############################################################

def extractBins(dataarray):
    '''Takes dataarray with storedhistogram and returns bins.'''
    unpackedDataarray = dataarray
    # This if statement allows to use the function directly or with apply_ufunc
    if isinstance(dataarray, xr.DataArray):
        unpackedDataarray = dataarray.item()
    bins = unpackedDataarray[1]
    return bins

def extractWeights(dataarray):
    '''Takes dataarray with stored histogram and returns weights.'''
    unpackedDataarray = dataarray
    # This if statement allows to use the function directly or with apply_ufunc
    if isinstance(dataarray, xr.DataArray):
        unpackedDataarray = dataarray.item()
    weights = unpackedDataarray[0]
    return weights

def getDeviationFromUniform(weights, perBin=False, perExp=False):
    '''How much a histogram deviates from a uniform distribution.

    Calculates the standard deviation ``stddev`` in all bins from the
    average value. With the number of bins ``n``::

        mean = 1/n * sum_i(w_i)
        stddev = \sqrt{\sum_i (w_i - mean)**2}

    Args:
        perBin (bool): This argument only matters if ``perExp`` is
          False. If ``perBin`` is False, the return value is ``stddev``,
          otherwise ``stddev/n``.
        perExp (bool): If True, the return value is divided by the
          expected standard deviation assuming that the bins are filled
          with random variables from a uniform distribution. See the
          related method :py:meth:`getExpectedDeviationFromUniform`. If
          this option is used, the argument ``perBin`` is irrelevant.
    Returns:
        float: ``stddev``, ``stddev/n`` or ``stddev / stddev_exp``.
    '''
    sumWeights = sum(weights)
    n = len(weights)
    mean = sumWeights / n
    stddev = np.sqrt(sum([(x-mean)**2 for x in weights]))
    if perExp:
        return stddev / getExpectedDeviationFromUniform(weights, perBin=False)
    if perBin:
        return stddev / n
    return stddev

def getExpectedDeviationFromUniform(weights, perBin=False):
    '''Calculate expected deviation from a uniform distribution.

    This is the equivalent method of :py:meth:`getDeviationFromUniform`.
    Assuming the entries of ``weights`` are drawn from a uniform
    distribution, calculate the expectation value for the standard
    deviation defined in :py:meth:`getDeviationFromUniform`. It only
    depends on the number of bins ``n`` and the overall sum of entries
    in ``weights``, ``N``. In each bin, a binomial distribution is
    expected leading to the expectation value::

        stddev_exp = sqrt(N * (1-1/n))

    Args:
        perBin (bool): If True, returns ``stddev_exp/n``
    Returns:
        float: ``stddev_exp`` or ``stddev_exp/n``
    '''
    N = sum(weights)
    n = len(weights)
    stddev_exp = np.sqrt(N * (1-1/n))
    if perBin:
        return stddev_exp/n
    return stddev_exp

def passWeightsToFunc(func, dataarray, kwargs):
    weights = xr.apply_ufunc(extractWeights, dataarray, vectorize=True, output_dtypes=[np.dtype('object')])
    return xr.apply_ufunc(func, weights, vectorize=True, kwargs=kwargs)

def main(args):

    conf = utils.importModuleFromConfig(args.config)
    log.INFO(f"Imported config from {args.config}")

    inputFile = utils.getInputPath(conf.computedOutputFile)
    ds = utils.readDataset(inputFile)


    plot.FFE_fromDataset(ds.loc[{"Nl": 16, "Nlr": 8, "Nlr_unc": 4}], saveAs="example1.pdf")
    plt.clf()
    plot.randomVsPValue_fromDataset(ds.loc[{"Nl": 16, "Nlr": 8, "Nlr_unc": 4}], saveAs="example2.pdf")
    # plt.show()

    # print(ds)

    # d_Nlr = [
    #     {"Nlr": 8},
    #     {"Nlr": 10},
    #     {"Nlr": 12},
    #     ]
    # d_Nlr_unc = [
    #     {"Nlr_unc": 1},
    #     {"Nlr_unc": 2},
    #     {"Nlr_unc": 4},
    #     {"Nlr_unc": 6},
    #     {"Nlr_unc": 8},
    #     ]
    # ds_squeezed = ds.squeeze()
    # for d1 in d_Nlr:
    #    for d2 in d_Nlr_unc:
    #        c = ds_squeezed.loc[d1].loc[d2]
    #        plot.FFE_fromDataset(c, saveAs=f"plots/plot_{d1['Nlr']}_{d2['Nlr_unc']}.pdf")


    bias = ds["mean"] - ds.trueValue
    bias.loc[{"Nl": 16}].plot(cmap="Oranges")
    # plt.show()
    relBias = bias / ds.trueValue
    relBias.loc[{"Nl": 16}].plot(cmap="Oranges")
    # plt.show()

    utils.decorateDataset(ds, "analyze", args)

if __name__ == "__main__":
    parser = utils.getArgParserAnalyze()
    args = parser.parse_args()
    main(args)
