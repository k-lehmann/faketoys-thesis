from faketoys import log, utils
import xarray as xr

preparedOutputFile = "prepared_1l_Nl_2d_pValue.pkl"
computedOutputFile = "computed_1l_Nl_2d_pValue.pkl"

smartBins_random = True
smartBins_nBins = 100
smartBins_nStddev = 4

pValueHistStepX = 0.02

compute_nRandom = int(1e6)

def getDataArray():
    bins = {}
    bins["FF_Nt"]      = [400]
    bins["FF_Ntr"]     = [0]
    bins["FF_Ntr_unc"] = [5]
    bins["FF_Nl"]      = [2000]
    bins["FF_Nlr"]     = [0]
    bins["FF_Nlr_unc"] = [40]

    bins["Nl"]         = [100, 200, 300, 500, 700, 1000, 1500, 3000, 5000, 10000]
    bins["Nlr"]        = [0]
    bins["Nlr_unc"]    = [0, 20, 40, 60, 80, 100, 150, 200, 300]

    dimensions = [x for x in bins.keys()]

    array = xr.DataArray(dims=tuple(dimensions), coords=bins)
    array = array.astype("object")
    utils.initializeArray(utils.getFFE_1l, array, denominatorGreaterThan=20)

    return array
