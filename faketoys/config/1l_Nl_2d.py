from faketoys import log, utils
import xarray as xr

i = 0

randomHistLowX = -10
randomHistHighX = 80
randomHistStepX = 1
pValueHistStepX = 0.02

preparedOutputFile = "prepared_1l_Nl_3d.pkl"
computedOutputFile = "computed_1l_Nl_3d.pkl"

compute_nRandom = int(1e6)

def getDataArray():
    bins = {}
    bins["FF_Nt"]      = [125]#, 150, 175]
    bins["FF_Ntr"]     = [75]#, 100, 125]
    bins["FF_Ntr_unc"] = [10]
    bins["FF_Nl"]      = [250]#, 300, 400]
    bins["FF_Nlr"]     = [50]#, 70, 90]
    bins["FF_Nlr_unc"] = [40]

    bins["Nl"]         = [0, 8, 16, 24, 32, 40, 80, 120]
    bins["Nlr"]        = [0]
    bins["Nlr_unc"]    = [0, 4,  8, 12, 16, 20, 40, 80]

    dimensions = [x for x in bins.keys()]

    array = xr.DataArray(dims=tuple(dimensions), coords=bins)
    array = array.astype("object")
    utils.initializeArray(utils.getFFE_1l, array)

    return array
