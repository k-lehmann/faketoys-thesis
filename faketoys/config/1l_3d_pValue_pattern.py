from faketoys import log, utils
import xarray as xr

filename = __file__.split("/")[-1]
filename = ".".join(filename.split(".")[0:-1])
preparedOutputFile = f"prepared_{filename}.pkl"
computedOutputFile = f"computed_{filename}.pkl"

smartBins_random = True
smartBins_nBins = 100
smartBins_nStddev = 4

pValueHistStepX = 0.02

compute_nRandom = int(1e6)

def getDataArray():
    bins = {}
    bins["FF_Nt"]      = [500]
    bins["FF_Ntr"]     = [0]
    # Can take bins at 5 and 75 from 1l_4d_pValue_2.py
    bins["FF_Ntr_unc"] = [0, 10, 20, 30, 40, 50, 60]
    bins["FF_Nl"]      = [500000]
    bins["FF_Nlr"]     = [0]
    bins["FF_Nlr_unc"] = [40]

    bins["Nl"]         = [500, 1000, 2000, 4000, 6000]
    bins["Nlr"]        = [0]
    bins["Nlr_unc"]    = [0, 15, 30, 45, 60, 100, 150, 200]

    dimensions = [x for x in bins.keys()]

    array = xr.DataArray(dims=tuple(dimensions), coords=bins)
    array = array.astype("object")
    utils.initializeArray(utils.getFFE_1l, array, denominatorGreaterThan=20)

    return array
