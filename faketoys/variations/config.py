from faketoys import log
from faketoys.stats import dist_base
from faketoys.variations import model_base

class VariationConfig(log.Logger):
    """Configuration of systematic variations.

    When a variable depends on a systematic uncertainty, this systematic
    uncertainty can be broken down into two parts. Firstly, the random
    choice of the value of the systematic uncertainty. Secondly, the
    dependence of the variable on the systematic uncertainty. Instances
    of :py:class:`VariationConfig` hold both of these: an instance of
    :py:class:`~faketoys.stats.dist_base.DistributionBase` and an
    instance of
    :py:class:`~faketoys.variations.model_base.VariationModelBase`.

    To model dependence of different variables on the same systematic
    uncertainties correctly, the distributions need to return the same
    value within each pseudo experiment. When the method drawVariation
    is called, the cacheIndex is forwarded to the distribution to make
    sure that all VariationConfigs using the same distribution return
    consistent values (which can be different, of course, depending on
    the variation model).
    """

    def __init__(self, distribution, variationModel, name=None):
        super().__init__(name=name)
        if not isinstance(distribution, dist_base.DistributionBase):
            VariationConfig.ERROR("Argument distribution has to be a DistributionBase")
        if not isinstance(variationModel, model_base.VariationModelBase):
            VariationConfig.ERROR("Argument variationModel has to be a VariationModelBase")
        self.distribution = distribution
        self.variationModel = variationModel
        if not self.checkConsistency():
            VariationConfig.WARNING(f"{self} has a systematic variation that is nominally not 0. This can cause weird behaviour, eg. that the nominal value of the parent distribution is shifted if evaluated with (systSources = []).")
        return

    def checkConsistency(self):
        '''Check consistency of distribution and model.

        If the underlying distribution assumes the true value, the
        variation should be zero. If this is not the case, return False.

        Returns:
            bool: True if distribution and model are consistent. False
            otherwise.
        '''
        trueValue = self.distribution.getTrueValue()
        variation = self.variationModel._calculateVariationExample(trueValue)
        if not variation == 0:
            return False
        return True

    def getDistribution(self):
        return self.distribution

    def getVariationModel(self):
        return self.variationModel

    def findAllSources(self):
        '''Finds recursively all sources of uncertainty.

        Returns:
            list: sources of systematic uncertainties
        '''
        return self.distribution.findAllSources()

    def getExpression(self, expression, statOnly=False):
        systExpression = self.distribution.getExpression(statOnly=statOnly)
        VariationConfig.DEBUG(f"Expression of systematic variation: {systExpression}")
        variationExpression = self.variationModel.calculateVariation(systExpression, nominalValue=expression)
        if variationExpression.nominal_value != 0:
            VariationConfig.WARNING(f"{self} has a non-zero effect on the nominal value of the expression {variationExpression.nominal_value} (should be 0).")
        VariationConfig.DEBUG(f"Returning updated expression {variationExpression}")
        return variationExpression

    def getVariations(self, systSource, cacheIndex=None, **kwargs):
        """Calculate effect of systematic variations.

        Calculate the variation of a parent distribution caused by the
        variation of the underlying distribution due to one systematic
        source. Typically, this would be an up/down variation. Keyword
        arguments are passed to the method calculateVariation of the
        underlying VariationModel.

        Args:
            systSource (list): Systematic variations to be varied.
            cacheIndex (int): Use cached values with `cacheIndex`.
            **kwargs: Arguments forwarded to
              :py:meth:`calculateVariations`
        Returns:
            list: Values of variations.
        """
        if systSource == self.getDistribution():
            VariationConfig.DEBUG(f"(getVariations) Found systematics source {systSource}")
            variedValues = self.getDistribution().getVariedValuesForStatUnc(cacheIndex=cacheIndex)
            VariationConfig.DEBUG(f"(getVariations) Got variedValues {variedValues}")
        else:
            variedValues = self.getDistribution().getVariedValuesForOneSystSource(systSource=systSource, cacheIndex=cacheIndex)

        variations = []
        for variedValue in variedValues:
            variations.append(self.variationModel.calculateVariation(variedValue, **kwargs))

        VariationConfig.DEBUG(f"(getVariations) After applying variationModel {self.variationModel}, returning variations {variations}")
        return variations

    def printVariationTree(self, level=0, depth=100):
        '''See :py:meth:`~faketoys.stats.dist_base.DistributionBase.printVariationTree`.'''
        self.getDistribution().printVariationTree(level=level+1, depth=depth)

    def printRandomTree(self, cacheIndex=None, systSources=[], level=0, depth=100):
        '''See :py:meth:`~faketoys.stats.dist_base.DistributionBase.printRandomTree`.'''
        s = level*"  "
        rand = self.drawVariation(cacheIndex=cacheIndex, systSources=systSources)
        print(f"{s}{self} (rand={rand})")
        self.getDistribution().printRandomTree(cacheIndex=cacheIndex, systSources=systSources, level=level+1, depth=depth)

    def drawVariation(self, cacheIndex=None, systSources=None, expression=False, rng=None, **kwargs):
        """Draw the random variation of this variation.

        Calculate the variation caused by the statistical fluctuation of
        the underlying distribution and by systematic sources (if
        specified in arguments). Keyword arguments are passed to the
        method calculateVariation of the underlying VariationModel.
        """
        VariationConfig.DEBUG("Entering drawVariation()")
        statUnc = False
        if cacheIndex == None:
            VariationConfig.WARNING("It is highly recommended to use caching to keep track of correlations between uncertainties.")
        if systSources == None:
            VariationConfig.DEBUG(f"No systSources were specified. Going to vary {self.distribution}, but no underlying systematics.")
            statUnc = True
            systSources = []

        # Evaluate the uncertainty of the underlying distribution only
        # if it is listed in the systSources. Otherwise, only evaluate
        # the impact of the systSources underlying to that.
        if self.distribution in systSources:
            statUnc = True
            VariationConfig.DEBUG(f"Planning to evaluate the uncertainty due to {self.distribution}, because it's in the list of systSources.")
        x = self.distribution.getRandom(cacheIndex=cacheIndex, systSources=systSources, statUnc=statUnc, expression=expression, rng=rng)
        VariationConfig.DEBUG(f"The systematic uncertainty {self.distribution} fluctuates to {x}")
        if len(self.distribution.getVariationConfigs()) == 0 and statUnc == False and x != 0:
            VariationConfig.WARNING(f"The {self} caused a variation, even though {self.distribution} should not be affected by varying the systSources {systSources}. Is this related to a systematic variation, whose nominal value causes a variation? If so, see the warning message above, else this is unexpected behaviour.")
        variation = self.variationModel.calculateVariation(x, **kwargs)
        VariationConfig.DEBUG(f"The affected random variable is varied by {variation}")
        return variation

    def __str__(self):
        return self.__repr__()
    def __repr__(self):
        if self.name == None:
            return f"VariationConfig for '{self.distribution}' with model {self.variationModel}"
        else:
            return self.name
