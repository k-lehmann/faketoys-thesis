from faketoys.variations import model_base

class AbsoluteVariation(model_base.VariationModelBase):
    '''Absolute variation.

    This class models a variation that depends linearly on the
    systematic uncertainty. The variation is given by::

        variation = factor * rand

    where `factor` is a member variable of this class and determines how
    large the impact of the systematic variation is. The variable `rand`
    is the random variable modelling the systematic uncertainty.
    '''
    def __init__(self, factor=1., name=None):
        super().__init__(name=name)
        self.factor = factor
        return

    def calculateVariation(self, x, **kwargs):
        AbsoluteVariation.DEBUG(f"Calculating {self.factor} * {x}")
        return self.factor * x

    def __str__(self):
        return self.__repr__()
    def __repr__(self):
        return f"AbsoluteVariation"



class SymmetricLinearVariation(model_base.VariationModelBase):
    """Symmetric linear variation as fraction of a value.

    This class models a relative, linear, systematic dependence. It is
    similar to :py:class:`AbsoluteVariation`, but also depends linearly
    on the true/random value (called nominal value) of the dependent
    distribution. Thus, it effectively models a relative uncertainty
    that scales with the random value::

        variation = factor * nominalValue * rand

    `factor` and `rand` are like in :py:class:`AbsoluteVariation`. The
    variable `nominalValue` needs to be provided to
    :py:meth:`calculateVariation`.
    """

    def __init__(self, factor, name=None):
        super().__init__(name=name)
        self.factor = factor
        return

    def calculateVariation(self, x, nominalValue):
        '''Calculate value of variation.

        Args:
            x (float): Random value drawn from systematic uncertainty.
            nominalValue (float): true or random value, for which
              relative uncertainty should be calculated.
        Returns:
            float: factor * nominalValue * x
        '''
        SymmetricLinearVariation.DEBUG(f"Calculating variation for {self}: {self.factor} * {nominalValue} * {x}")
        retVal = self.factor * nominalValue * x
        SymmetricLinearVariation.DEBUG(f"Result: {retVal}")
        return retVal

    def _calculateVariationExample(self, x):
        '''See
        :py:class:~faketoys.variations.model_base.`VariationModelBase`.
        '''
        return self.calculateVariation(x, nominalValue=1)

    def __str__(self):
        return self.__repr__()
    def __repr__(self):
        return f"SymmetricLinearVariation({self.factor})"
