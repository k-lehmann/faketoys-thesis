import faketoys
from faketoys.stats import dists, derived_dists, dist_base
from faketoys import FakeFactorEquation, log
import os, os.path
import datetime
import importlib
import subprocess
import numpy as np
import argparse
import pickle
import xarray as xr
import uncertainties

def getArgParserPrepare():
    '''Get argument parser for prepare script.'''
    parser = argparse.ArgumentParser()
    parser.add_argument('config', type=str, help='config file with a getDataArray() method')
    return parser

def getArgParserCompute():
    '''Get argument parser for compute script.'''
    parser = argparse.ArgumentParser()
    parser.add_argument('config', type=str, help='config file with a getDataArray() method')
    parser.add_argument('--attachRandom', action='store_true', default=False, help='save unhistogrammed random variables to dataset (this can make the output huge)')
    parser.add_argument('--parallelize', action='store_true', default=False, help='parallelize simulation')
    parser.add_argument('--num_workers', metavar="N", type=int, default=-1, help='distribute jobs across N cores (only with --parallelize)')
    parser.add_argument('--progress_dt', metavar="DT", type=float, default=0.1, help='update progress bar every DT sec (only with --parallelize)')
    parser.add_argument('--random_seed', type=int, default=None, help='initialize random number generator with seed')
    return parser

def getArgParserAnalyze():
    '''Get argument parser for analyze script.

    Since analyze is not really used at the moment, this just returns
    the argparser for prepare.
    '''
    return getArgParserPrepare()

def getArgParserSubmit():
    '''Get argument parser for submit script.'''
    parser = argparse.ArgumentParser()
    parser.add_argument('config', type=str, help='config file with a getDataArray() method')
    parser.add_argument('--time', type=int, default=60, help='time limit in minutes')
    parser.add_argument('--num_workers', metavar="N", type=int, help='distribute jobs across N cores', required=True)
    parser.add_argument('--progress_dt', metavar="DT", type=float, default=-1, help='update progress bar every DT sec')
    parser.add_argument('--random_seed', type=int, default=None, help='initialize random number generator with seed')
    return parser

def readDataset(inputFile, infoMessage=True):
    '''Reads a pickled xarray dataset from file.

    If the pickled content is not a dataset, a warning is emitted.

    Args:
        inputFile (str): Path to an input file. This can be any path
          understood by python's built-in ``open()``. It is advisable
          to use an absolute path. See :py:meth:`getInputPath`.
        infoMessage (bool): An info message about the dataset is
          printed. Default: True.
    Returns:
        Contents of pickled file.
    '''
    with open(inputFile, 'rb') as f:
        ds = pickle.load(f)
    if not isinstance(ds, xr.core.dataarray.Dataset):
        log.WARNING(f"Object read from file is not an xarray dataset.")
    elif infoMessage:
        if 'processedBy' in ds.attrs:
            l = ", ".join(ds.attrs['processedBy'])
            log.INFO(f"Read dataset from file that has been processed by {l}")
        else:
            log.INFO(f"Read file from {inputFile}")
    return ds

def getInputPath(filename):
    '''Find the specified file relative to ``$FAKEOUTPUT``.

    All scripts are set up to write output files into the directory
    specified in the environment variable ``$FAKEOUTPUT``. This method
    resolves any filename relative to this path and returns an absolute
    path. A path is also returned if there is no such file.

    If ``$FAKEOUTPUT`` is not defined, this method returns the argument
    ``filename``.

    See related method :py:meth:`getOutputPath`.

    Args:
        filename (str): Filename to be resolved as absolute path.
    Returns:
        str: Filename as an absolute path.
    '''
    if "FAKEOUTPUT" in os.environ:
        fakeOutput = os.environ["FAKEOUTPUT"]
        outPath = os.path.join(fakeOutput, filename)
        return outPath
    return filename

def getOutputPath(filename):
    '''Get an absolute filename relative to ``$FAKEOUTPUT``.

    Given a filename, this method returns an absolute path relative to
    the environment variable ``$FAKEOUTPUT``. If the directory does not
    exist, it is created. Use this method when writing files to have
    them all in the same location.

    See related method :py:meth:`getInputPath`.

    Args:
        filename (str): Filename to be written to.
    Returns:
        str: Filename as an absolute path.
    '''
    if "FAKEOUTPUT" in os.environ:
        fakeOutput = os.environ["FAKEOUTPUT"]
        outPath = os.path.join(fakeOutput, filename)
        outDir = "/".join(outPath.split("/")[:-1])
        if not os.path.isdir(outDir):
            os.mkdir(outDir)
        return outPath
    return filename

def resolveConfigPath(filepath):
    '''Takes a relative/absolute config path and returns absolute path.

    Args:
        filepath (str): Filepath relative to current working directory,
          relative to the faketoys package or an absolute path.
    Returns:
        str: Absolute path to the config. ``None`` if relative path and
        the file is not found.
    '''
    if filepath.startswith("/"):
        return filepath

    cwd = os.getcwd()
    absPath = "/".join([cwd, filepath])
    if os.path.isfile(absPath):
        return absPath

    log.WARNING(f"File '{absPath}' does not exist. Trying to find '{filepath}' relative to 'faketoys' instead.")
    if not "FAKEBASE" in os.environ:
        log.ERROR(f"Please define environment variable FAKEBASE. Returning None.")
        return None
    faketoysPath = os.environ["FAKEBASE"] + "/faketoys"
    absPath = "/".join([faketoysPath, filepath])

    if os.path.isfile(absPath):
        return absPath

    log.ERROR(f"Cannot resolve '{filepath}'. Returning None.")
    return None

def importModuleFromConfig(filepath):
    '''Import a python module from a file.

    The module path is resolved by :py:meth:`resolveConfigPath`. Then
    the module is read and returned.

    Args:
        filepath (str): filename/path to the module.
    Returns:
        Imported module.
    '''
    absPath = resolveConfigPath(filepath)
    if absPath == None:
        log.ERROR(f"Couldn't find config file '{filepath}'. Returning None.")
        return None

    filename = absPath.split("/")[-1]
    moduleName = ".".join(filename.split(".")[0:-1])
    if not filename.endswith(".py"):
        log.WARNING(f"Expecting python file with ending '.py' as config ('{moduleName}').")

    spec = importlib.util.spec_from_file_location(moduleName, absPath)
    conf = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(conf)

    if isinstance(conf.compute_nRandom, float) and conf.compute_nRandom.is_integer():
        conf.compute_nRandom = int(conf.compute_nRandom)
    return conf

def executeCommand(command, printErrorMessage=True):
    '''Execute command in shell.

    Args:
        command (str): Command to be executed.
        printErrorMessage (bool): Whether to print the message from the
          error stream if it's not empty.
    Returns:
        tuple: stdout, stderr, return code
    '''
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, executable='/bin/bash')
    out, err = p.communicate()
    out = out.decode("ascii")
    err = err.decode("ascii")
    if err != "" and printErrorMessage:
        log.ERROR("ERROR MESSAGE:"+ err)
    return out, err, p.returncode

def executeGitCommand(command, git_dir="", printErrorMessage=True):
    '''Execute git command in shell.

    Args:
        command (str): Command to be executed in git. Use the same
          syntax as in shell, but without leading ``git``.
        printErrorMessage (bool): Whether to print the message from the
          error stream if it's not empty.
    Returns:
        tuple: stdout, stderr, return code
    '''
    if git_dir == "":
      newCommand = "git " + command
    else:
      newCommand = "git --git-dir=" + git_dir + " " + command
    return executeCommand(newCommand, printErrorMessage)

def getGitHashHEAD():
    '''Get hash of the current HEAD in git.

    Returns:
        str: Git hash.
    '''
    out, err, returnCode = executeGitCommand("rev-parse HEAD")
    return out.strip()

def haveGitFilesChanged():
    '''Returns true if git files are different from current HEAD.'''
    out, err, returnCode = executeGitCommand("diff --exit-code")
    out1, err1, returnCode1 = executeGitCommand("diff --cached --exit-code")
    if returnCode == 0 and returnCode1 == 0:
        return False
    return True

def getGitDiff():
    '''Get output of ``git diff``.'''
    out, err, returnCode = executeGitCommand("diff")
    return out

def decorateDataset(dataset, jobname, args, executionTime=True):
    '''Add useful meta information to dataset attributes.

    The information can be viewed with ``dataset.attrs``. The following
    information is stored:

    * the script that is processing the dataset,
    * the git commit that the code is currently on,
    * whether the git commit is clean or not,
    * the command line arguments,
    * the execution time.

    Note that the execution time is measured from when faketoys package
    is first imported until the call of this function. If this function
    is not called at the end of the script's runtime, consider passing
    the argument ``executionTime=False``.

    Args:
        dataset (xarray.Dataset): Dataset to be decorated.
        jobname (str): Name that identifies this job.
        args (Namespace): Command-line arguments to be recorded.
        executionTime (bool): Whether to save the execution time.
    '''
    if "processedBy" in dataset.attrs:
        dataset.attrs["processedBy"].append(jobname)
    else:
        dataset.attrs["processedBy"] = [jobname]
    dataset.attrs[jobname + "_gitHEAD"] = getGitHashHEAD()
    dataset.attrs[jobname + "_gitHEADClean"] = not haveGitFilesChanged()
    dataset.attrs[jobname + "_CLI"] = vars(args)
    if executionTime:
        now = datetime.datetime.now()
        seconds = (now - faketoys.startTime).total_seconds()
        execTime = datetime.timedelta(seconds=round(seconds))
        dataset.attrs[jobname + "_executionTime"] = str(execTime)

def getProduct(d, index=0, name=None):
    if name == None:
        name = f"eq{index}"
    listOfDist = []
    for key in d.keys():
        if key[0] == "G":
            newElement = dists.Gaussian(100, d[key])
        elif key[0] == "P":
            if d[key] == 0:
                # large number, but not too large to break anything
                # (even if multiplied by itself)
                N = 1e6
            else:
                N = 1/(1e-4 * d[key]*d[key])
            newElement = dists.Poisson(N)
        else:
            log.WARNING(f"Not sure which distribution to use for {key}.")
        listOfDist.append(newElement)
    return derived_dists.ProductOfDistributions(listOfDist, name=name)

def getFFE_1l(d, index=0, denominatorGreaterThan=-float("inf"), name=None):
    '''Use a dictionary to initialize 1-lepton FF equation.

    The dictionary ``d`` defines how the Fake Factor equation looks.
    Use the following keys with numbers as values:

    * Nl
    * Nlr
    * Nlr_unc
    * FF_Nt
    * FF_Ntr
    * FF_Ntr_unc
    * FF_Nl
    * FF_Nlr
    * FF_Nlr_unc

    Args:
        d (dict): True values for Poisson and Gaussian distributions.
        index (int): If ``name`` is not defined, name the new Fake
        Factor equation "FFE" + index.
        denominatorGreaterThan (float): Protection against small
          denominators.
        name (str): Name of Fake Factor equation.
    Returns:
        FakeFactorEquation

    '''
    # Dummy returns nan for the most part. It is used if the fake
    # factor equation does not make sense because it goes negative.
    if d["Nl"] < d["Nlr"]:
        return dists.Dummy()
    if d["FF_Nt"] < d["FF_Ntr"]:
        return dists.Dummy()
    if d["FF_Nl"] < d["FF_Nlr"]:
        return dists.Dummy()

    FFName = name
    if FFName == None:
        FFName = f"FFE{index}"

    Nt  = dists.Poisson(d['FF_Nt'])
    Ntr = dists.Gaussian(d['FF_Ntr'], d['FF_Ntr_unc'])
    Nl  = dists.Poisson(d['FF_Nl'])
    Nlr = dists.Gaussian(d['FF_Nlr'], d['FF_Nlr_unc'])
    F0  = derived_dists.FakeFactor(Nt=Nt, Ntr=Ntr, Nl=Nl, Nlr=Nlr, denominatorGreaterThan=denominatorGreaterThan)

    e = FakeFactorEquation.FakeFactorEquation(1, name=FFName)
    e.setFakeFactor(0, F0)

    e.setNEvents("" , "l", dists.Poisson(d["Nl"]))
    e.setNEvents("r", "l", dists.Gaussian(d["Nlr"], d["Nlr_unc"]))

    if not e.loadDistributions():
        log.WARNING(f"loadDistributions failed on configuration {d}")
    return e

autoInitializedIndex = 0

def initializeArray(function, array, subArray=None, **kwargs):
    '''Initialize an xarray.DataArray according to its coordinates.

    Use ``function`` to initialize each entry in the ``array``. The
    ``function`` gets the coordinates of the current entry and needs to
    return the object that is supposed to be stored in the array.

    Args:
        function (callable): Function constructing an entry in the
          array.
        array (xarray.DataArray): Empty array to be filled.
        subArray (xarray.DataArray): The function loops recursively over
          all entries in ``array``. When going in recursion,
          ``subArray`` refers to the currently relevant selection, but
          the new object is still assigned to ``array``. Use this
          argument if you only want to define a part of ``array``.
        **kwargs: Arguments to be passed to ``function``.
    '''
    if id(subArray) == id(None):
        subArray = array
    if len(subArray.dims) == 0:
        d = dict([(key,ary.values.item()) for key,ary in subArray.coords.items()])
        global autoInitializedIndex
        autoInitializedIndex += 1
        array.loc[d] = function(d, autoInitializedIndex, **kwargs)
        log.DEBUG(str(d) + " set!")
        return
    for subsub in subArray:
        initializeArray(function, array, subsub, **kwargs)

def divide(x, y):
    '''Divides x by y. But it returns NAN instead of infinity.'''
    if y == 0:
        return np.nan
    return x/y

def variablesEqual(x, y):
    '''Check if variables are equal (also works for ufloat).'''
    if isinstance(x, uncertainties.core.Variable):
        x = x.n
    if isinstance(y, uncertainties.core.Variable):
        y = y.n
    return x == y

def setRandomSeed(seed):
    '''Set the seed of the random number generator of DistributionBase.

    This is a wrapper around :py:meth:`faketoys.stats.dist_base.setRNG`.

    Args:
        seed (int): New seed. If `None` is passed, nothing is changed.
    Returns:
        bool: True if new seed was set, else False.
    '''
    if not seed == None:
        return dist_base.DistributionBase.setRNG(seed=seed)
    return False

def getCombinations(ds):
    '''Get all combinations of indices for xarray object as dict.'''
    comb = ds.coords.to_index(ds.dims)
    if len(ds.dims) == 1:
        for dim in ds.dims:
            return [{dim: x} for x in comb]
    return [dict(zip(ds.dims, x)) for x in comb]
