.. faketoys documentation master file, created by
   sphinx-quickstart on Wed Nov  4 16:39:37 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to faketoys's documentation!
====================================

.. toctree::
   :maxdepth: 4
   :caption: Contents:

   short_doc
   faketoys


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
